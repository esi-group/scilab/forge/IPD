function WriteImage(Image, ImagePath)
//////////////////////////////////////////////////////////////////////////// 
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////

 // Global variables used as constants are declared.

 global TYPE_INT;
 
 global TYPE_DOUBLE;
 
 global TYPE_BOOLEAN;
  
 // Input and output parameters are checked.
 
 [NumberOfOutputs NumberOfInputs] = argn();
 
 if NumberOfInputs ~= 2 then
  
  error('Wrong number of input parameters');
  
 end;
 
 // Image must be a 2D or 3D matrix.
 
 Dimensions = size(Image);
 
 NumberOfDimensions = length(Dimensions);
 
 if (NumberOfDimensions ~= 2) & (NumberOfDimensions ~= 3) then
   
  error('Image must be a 2D or a 3D matrix.');
   
 end;
 
 CheckString(ImagePath, 'ImagePath');
 
 // This function has no output parameters.
 
 if NumberOfOutputs > 1 then
 
  error('Wrong number of output parameters.');
 
 end;
 
 // The data type of Image is determined. If Image is of a type other than uint8, it
 // must be converted to uint8.
 
 if NumberOfDimensions == 3 then
   
  ImageType = type(Image(1));
   
 else
   
  ImageType = type(Image); 
    
 end;
  
 select ImageType
    
  case TYPE_BOOLEAN 
    
   PixelList = uint8(Image(:));
     
  case TYPE_DOUBLE
     
   PixelList = uint8(Image(:) * (2 ^ 8 - 1));
    
  case TYPE_INT
    
   if NumberOfDimensions == 3 then
       
    Precision = typeof(Image(1));
       
   else
     
    Precision = typeof(Image);
         
   end;
   
   select Precision
     
    case 'uint8'
      
     PixelList = Image(:);
      
    case 'uint16'
      
     PixelList = ScaleToUInt8(Image);
                 
    case 'uint32'
      
     PixelList = ScaleToUInt8(Image);
      
    else
      
     error('Image must be of type boolean, uint8, uint16, uint32 or double.');
      
   end;
   
  else
    
   error('Image must be of type boolean, uint8, uint16, uint32 or double.');
           
 end;
 
 if NumberOfDimensions == 2 then
   
  Dimensions = cat(2, Dimensions, 1);
   
 end;
 
 Success = WriteImageFile(PixelList, Dimensions, ImagePath);
 
 if Success == 0 then
   
  error("Error when trying to save image to file.");
   
 end;
   
endfunction
