function [IndexImage, ColorMap] = RGB2Ind(RGB)
//////////////////////////////////////////////////////////////////////////// 
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////

 // Global variables used as constants are declared.

 global TYPE_DOUBLE;

 // Input and output parameters are checked.
 
 [NumberOfOutputs NumberOfInputs] = argn();
 
 if NumberOfInputs ~= 1 then
  
  error('Wrong number of input parameters.');
  
 end;
 
 if length(size(RGB)) ~= 3 then
  
  error('Parameter RGB must be a 3D matrix.');
  
 end;
 
 if NumberOfOutputs ~= 2
  
  error('Wrong number of output parameters.');
  
 end;
   
 // Initialization
 
 PixelList = list();
  
 [NumberOfRows NumberOfColumns NumberOfChannels] = size(RGB);
 
 NumberOfPixels = NumberOfRows * NumberOfColumns;
 
 // The color map is a matrix containing each color vector present in RGB
 // as a row. 
 
 Sample = RGB(1);
 
 if type(Sample) == TYPE_DOUBLE
   
   ColorMap = matrix(RGB, NumberOfPixels, NumberOfChannels);
   
 else
   
   TypeName = typeof(Sample);
  
   select TypeName
   
    case 'uint8'
    
     MaxGrayValue = 2 ^ 8 - 1;
    
    case 'uint16'

     MaxGrayValue = 2 ^ 16 - 1;
    
    case 'uint32'
    
     MaxGrayValue = 2 ^ 32 - 1;
    
  end;
  
  ColorMap = double(matrix(RGB, NumberOfPixels, NumberOfChannels)) ...
           / MaxGrayValue;
  
 end;
 
 // Each pixel is mapped to its index in the color map.
 
 IndexImage = matrix(1 : NumberOfPixels, NumberOfRows, NumberOfColumns);
   
endfunction

