function BlobStatistics = AnalyzeBlobs(BlobImage, IsCalculated)
//////////////////////////////////////////////////////////////////////////// 
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////

 // Input and output parameters are checked.
 
 [NumberOfOutputs NumberOfInputs] = argn();
 
 if NumberOfInputs ~= 2 then
  
  error('Wrong number of input parameters');
  
 end;
 
 CheckNumericMatrix(BlobImage, 'BlobImage');
 
 CheckStruct(IsCalculated, 'IsCalculated');
  
 if NumberOfOutputs ~= 1 then 
  
  error('Wrong number of outputs.');
  
 end;
 
 // Initializations
 
 BlobStatistics = list();
 
 SizeHistogram = CreateHistogram(BlobImage);
 
 NumberOfObjects = length(SizeHistogram) - 1;
 
 // If there are no objects in BlobImage, the function returns.
 if NumberOfObjects == 0 then
    
  return;
  
 end;

 // Blob statistics is a struct of arrays.
  
 MaxArea = max(SizeHistogram(2 : $));
 
 [NumberOfRows NumberOfColumns] = size(BlobImage);
 
 NumberOfPixels = length(BlobImage);
 
 // The pixel index matrix is generated. 
 PixelIndexMatrix = CreatePixelIndexList(uint32(BlobImage));
 
 // For each objects the specified image features are calculated now.
 
 for n = 1 : NumberOfObjects
  
  ObjectInfo = struct();
  
  // PixelIndexList is always needed, even if it is not returned.
  PixelIndexList = PixelIndexMatrix(1 : SizeHistogram(n + 1), n);
      
  // PixelIndexList is inserted.
  if IsCalculated.PixelIndexList then
   
   ObjectInfo.PixelIndexList = PixelIndexList;
   
  end;
 
  // PixelList is calculated.
  if IsCalculated.PixelList then
   
   MirroredList = ind2sub([NumberOfRows NumberOfColumns], PixelIndexList);
   
   ObjectInfo.PixelList = zeros(length(PixelIndexList), 2);
   
   ObjectInfo.PixelList(:, 1) = MirroredList(:, 2);
   
   ObjectInfo.PixelList(:, 2) = MirroredList(:, 1);
                                   
  end;
 
  // BoundingBox is calculated. 
 
  if IsCalculated.BoundingBox then
   
   if IsCalculated.PixelList then
    
    PixelList = ObjectInfo.PixelList;
    
   else

    PixelList = zeros(length(PixelIndexList), 2);
    
    MirroredList = ind2sub([NumberOfRows NumberOfColumns], PixelIndexList);
    
    PixelList(:, 1) = MirroredList(:, 2);
   
    PixelList(:, 2) = MirroredList(:, 1);
                        
   end;
                       
   Left = min(PixelList(:, 1));
   
   Right = max(PixelList(:, 1));
   
   Top = min(PixelList(:, 2));
   
   Bottom = max(PixelList(:, 2));
   
   ObjectInfo.BoundingBox = [Left; ...
                             Top; ...
                             Right - Left; ...
                             Bottom - Top];
     
  end;
  
  // Centroid is calculated.
  if IsCalculated.Centroid then
    
   if IsCalculated.PixelList | IsCalculated.BoundingBox
     
    PixelList = ObjectInfo.PixelList; 
        
   else
     
    PixelList = zeros(length(PixelIndexList), 2);
    
    MirroredList = ind2sub([NumberOfRows NumberOfColumns], PixelIndexList);
    
    PixelList(:, 1) = MirroredList(:, 2);
   
    PixelList(:, 2) = MirroredList(:, 1); 
      
   end;
    
   ObjectInfo.Centroid = mean(PixelList, 'r');
    
  end;
  
  // All desired features are calculated for the current object.
  BlobStatistics($ + 1) = ObjectInfo;
  
 end;
 
endfunction

