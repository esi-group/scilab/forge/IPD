function CallbackImageFile(ItemIndex, WindowNumber)
/////////////////////////////////////////////////////////////////////////
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////

 // Constants that describe menu items are defined.
 
 FILE_OPEN = 1;

 FILE_SAVE = 2;

 FILE_CLOSE = 3;

 LIST_OF_FILE_TYPES = ['*.bmp', '(*.bmp) Bitmap'; ...
                       '*.jpg', '(*.jpg) JPEG'; ...
                       '*.png', '(*.png) Portable Network Graphics';
                       '*.tif', '(*.tif) Tagged Image File Format'];

 // Further initializations
 
 FigureHandle = findobj('figure_id', WindowNumber);
 
 // Action is carried out.
 
 select ItemIndex

  case FILE_OPEN

   select getlanguage()

    case 'de_DE'

     Title = 'Bilddatei öffnen';

    case 'en_US'

     Title = 'Open Image File';

    case 'ja_JP'

     Title = '画像のファイルを開く';

    else

     Title = 'Open Image File';

   end;

   [FileName Path] = uigetfile(LIST_OF_FILE_TYPES, ...
                               FigureHandle.user_data.Path, ...
                               Title);

   if (FileName == '') | (Path == '') then

    return;

   end;

   for n = 1 : length(FigureHandle.user_data.ListOfDiagrams)

    if is_handle_valid(FigureHandle.user_data.ListOfDiagrams(n)) then

     delete(FigureHandle.user_data.ListOfDiagrams(n));

    end;

   end;

   Image = ReadImage(Path + filesep() + FileName);

   set(FigureHandle, 'event_handler_enable', 'off');

   scf(FigureHandle);

   select length(size(Image))

    case 2

     ShowImage(Image, FileName);

    case 3

     ShowColorImage(Image, FileName);

   end;

   ImageArea = FigureHandle.children(1);

   ImageArea.margins = [0 0 0 0];

   ImageArea.box = 'hidden_axes';

   set(FigureHandle, ...
       'UserData', ...
       struct('Path', ...
              Path, ...
              'ImageType', ...
              type(Image(1)), ...
              'ImageSize', ...
              size(Image), ...
              'Image', ...
              Image, ...
              'SelectedPixel', ...
              [], ...
              'SelectedLine', ...
              [], ...
              'SelectedRectangle', ...
              [], ...
              'StartPoint', ...
              [], ...
              'EndPoint', ...
              [], ...
              'ListOfDiagrams', ...
              list()));

   NumberOfMenus = length(FigureHandle.children) - 1;

   for n = 3 : NumberOfMenus

    Menu = FigureHandle.children(n);

    for m = 2 : length(Menu.children)

     unsetmenu(WindowNumber, Menu.label, m);

    end;

   end;

   set(FigureHandle, 'event_handler_enable', 'on');

  case FILE_SAVE

   select getlanguage()

    case 'de_DE'

     Title = 'Bilddatei speichern';

    case 'en_US'

     Title = 'Save Image File';

    case 'ja_JP'

     Title = '画像のファイルを蓄積する';

    else

     Title = 'Save Image File';

   end;

   [FileName Path Index] = uiputfile(LIST_OF_FILE_TYPES, ...
                                     FigureHandle.user_data.Path, ...
                                     Title);

   if Index == 0 then

    return;

   end;

   Ending = part(LIST_OF_FILE_TYPES(Index), ...
                 2 : length(LIST_OF_FILE_TYPES(Index)));

   if isempty(strindex(FileName, Ending)) then

    FileName = FileName + Ending;

   end;

   WriteImage(FigureHandle.user_data.Image, Path + filesep() + FileName);

  case FILE_CLOSE

   for n = 1 : length(FigureHandle.user_data.ListOfDiagrams)

    if is_handle_valid(FigureHandle.user_data.ListOfDiagrams(n)) then

     delete(FigureHandle.user_data.ListOfDiagrams(n));

    end;

   end;

   delete(FigureHandle);
   
 end;

endfunction