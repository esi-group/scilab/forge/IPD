function ExportAsGlobalVariable(Data, Title, Label, Message)
/////////////////////////////////////////////////////////////////////////
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////


 VariableName = x_mdialog(Title, Label, '');
 
 if isempty(VariableName) then

  return;

 end;

 Declaration = 'global ' + VariableName + ';';

 try

  execstr(Declaration);

 catch

  select getlanguage()

   case 'de_DE'

    WindowTitle = 'Fehler';

    Message = 'Der eingegebene Variablenname ist ungültig.';

   case 'en_US'

    WindowTitle = 'Error';

    Message = 'The input is not a valid variable name.';

   case 'ja_JP'

    WindowTitle = 'エラー';

    Message = '入力された文字列は変数の名前として使われない。';

   else

    WindowTitle = 'Error';

    Message = 'The input is not a valid variable name.';

  end;

  messagebox(Message, WindowTitle, 'error', 'OK', 'modal');

  return;

 end;

 Assignment = VariableName + ' = Data';
 
 execstr(Assignment);

 disp(VariableName);

 disp(Message);

endfunction