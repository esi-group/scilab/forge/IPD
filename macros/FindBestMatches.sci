function BestMatchList = FindBestMatches(Match, TemplateSize, Method)
///////////////////////////////////////////////////////////////////////
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////

 // Global variables used as constants are declared.
 
 global TYPE_DOUBLE;

 global MATCH_LEAST_SQUARES;
 
 global MATCH_CORRELATION;
 
 global MATCH_LEAST_SQUARES_NORM;
 
 global MATCH_CORRELATION_NORM;
  
 // Input and output parameters are checked.

 [NumberOfOutputs NumberOfInputs] = argn();
 
 if NumberOfInputs ~= 3 then
   
  error('Wrong number of input arguments.');
   
 end;
 
 if NumberOfOutputs ~= 1 then
   
  error('Wrong number of outputs.');
   
 end;
 
 // The first parameter must be a 2D matrix of type double.
 
 CheckNumericMatrix(Match, 'Match');
 
 if type(Match) ~= TYPE_DOUBLE then
   
  error('Match must be a 2D matrix of type double.');
   
 end;
 
 // The second parameter must be a vector with exactly 2 elements.
 
 CheckVector(TemplateSize, 'TemplateSize');
 
 if (length(TemplateSize) ~= 2) ...
 |  (TemplateSize(1) ~= round(TemplateSize(1))) ...
 |  (TemplateSize(2) ~= round(TemplateSize(2))) then
   
  error('TemplateSize must be a vector with exactly two int ' + ...
        ' elements.');   
   
 end;
 
 // The third parameter must be a scalar of type uint8.
 
 if (typeof(Method) ~= 'uint8') | (length(Method) ~= 1) then
   
  error('Method must be a scalar of type uint8.'); 
      
 end;
 
 // The optimal match values are searched based on the template
 // matching method.
 
 select Method 
   
  case MATCH_LEAST_SQUARES
    
   IndexList = find(Match == min(Match));
    
  case MATCH_CORRELATION
    
   IndexList = find(Match == max(Match));
    
  case MATCH_LEAST_SQUARES_NORM
    
   IndexList = find(Match == min(Match));
        
  case MATCH_CORRELATION_NORM  
        
   IndexList = find(Match == max(Match));
     
 end;
 
 PixelList = ind2sub(size(Match), IndexList) ...
           + floor(TemplateSize / 2);
 
 BestMatchList = zeros(size(PixelList, 1), 2);
 
 BestMatchList(:, 1) = PixelList(:, 2);
 
 BestMatchList(:, 2) = PixelList(:, 1);
  
endfunction
