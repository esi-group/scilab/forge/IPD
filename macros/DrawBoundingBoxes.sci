function DrawBoundingBoxes(BlobList, Color, FigureHandle)
///////////////////////////////////////////////////////////////////////
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////


 // Global variables used as constants are declared.
 
 global TYPE_LIST;
  
 // Input and output parameters are checked.
 
 [NumberOfOutputs NumberOfInputs] = argn();
 
 if (NumberOfInputs < 2) | (NumberOfInputs > 3) then
  
  error('Wrong number of input parameters.');
  
 end;
 
 if type(BlobList) ~= TYPE_LIST then
  
  error('BlobList must be of type list.');
  
 end;

 CheckVector(Color, 'Color');
 
 if (min(Color) < 0) | (max(Color) > 1) then
  
  error('All elements of Color must be in the interval [0, 1].');
  
 end;
  
 if NumberOfInputs == 3 then
  
  CheckGraphicHandle(FigureHandle, 'FigureHandle');
  
 else
  
  FigureHandle = gcf();
  
 end;
 
 if NumberOfOutputs > 1 then
  
  error('Wrong number of outputs.');
  
 end;
 
 // Color is searched in the color map of FigureHandle. If Color is
 // not found, it is appended to the color map. 
 
 ColorMap = FigureHandle.color_map;
 
 ColorIndices = vectorfind(ColorMap, Color, 'r');
 
 if isempty(ColorIndices) then
   
  ExtendedColorMap = zeros(size(ColorMap, 1) + 1, size(ColorMap, 2));
  
  ExtendedColorMap(1 : $ - 1, :) = ColorMap;
  
  ExtendedColorMap($, :) = Color;
  
  ColorIndex = size(ExtendedColorMap, 1);
  
  FigureHandle.color_map = ExtendedColorMap;
  
 else
  
  ColorIndex = ColorIndices(1);
  
 end;
 
 
 // The matrix of rectangles is generated. Each bounding box
 // corresponds to a column or the rectangle matrix. If BlobList 
 // is empty, this matrix is not defined and the function returns.
 
 NumberOfBlobs = length(BlobList);
 
 if NumberOfBlobs == 0 then
  
  return;
  
 end;
 
 RectangleMatrix = zeros(4, NumberOfBlobs);
 
 for n = 1 : NumberOfBlobs
  
  RectangleMatrix(:, n) = BlobList(n).BoundingBox;
  
 end;

 Diagram = gca();
 
 NumberOfRows = Diagram.data_bounds(4) - Diagram.data_bounds(3);

 RectangleMatrix(2, :) = NumberOfRows - RectangleMatrix(2, :) + 1;
 
 // The bounding boxes are drawn now.
 
 scf(FigureHandle);
 
 xrects(RectangleMatrix, -ColorIndex * ones(1, NumberOfBlobs));
  
endfunction

