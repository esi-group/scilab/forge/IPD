function FigureHandle = ShowImage(Image, Title, ColorMap)
//////////////////////////////////////////////////////////////////////////// 
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
// Allan CORNET - 2012
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////

 // Global variables used as constants are declared.
 
 global TYPE_INT;

 global TYPE_DOUBLE;

 global TYPE_BOOLEAN;
 
 // Parameters are checked.
 
 [NumberOfOutputs NumberOfInputs] = argn();

 // There must be at least two input parameters and at most three.
 
 if (NumberOfInputs < 2) | (NumberOfInputs > 3) then
  
  error('Wrong number of input parameters.');
  
 end;
 
 // Image must be a 2D matrix.
 
 CheckMatrix(Image, 'Image');

 // Title must be a string.
 
 CheckString(Title, 'Title');
  
 // ColorMap is optional. If ColorMap is specified, it must be a matrix with 
 // three columns. If ColorMap is not specified, it is initialized to gray
 // color map.
 
 MaxUInt8 = 2 ^ 8 - 1;
 
 if NumberOfInputs == 3
 
  if ndims(ColorMap) == 2 then
   
   if size(ColorMap, 2) ~= 3
    
    error('ColorMap must be a 2D matrix with exactly three columns.');
    
   end;
          
  else
  
   error('ColorMap must be a 2D matrix with exactly three columns.');
  
  end;
 
 else
   
  if type(Image) == TYPE_INT then
    
   select typeof(Image)
     
    case 'uint8'
      
     MaximumGrayValue = MaxUInt8;
     
    case 'uint16'
      
     MaximumGrayValue = max(Image(:));
     
    case 'uint32'
      
     MaximumGrayValue = max(Image(:));
                
   end;
       
  else
    
   MaximumGrayValue = MaxUInt8;
            
  end; 
    
  ColorMap = graycolormap(double(MaximumGrayValue + 1));
 
 end;
 
 FigureHandle = gcf();
 drawlater();
 
 FigureHandle.color_map = ColorMap;
 
 FigureHandle.background = -2; // sets the background to white
 
 FigureHandle.figure_name = Title;
 
 [NumberOfRows NumberOfColumns] = size(Image);
 
 FigureHandle.axes_size = [NumberOfColumns NumberOfRows];
 
 delete(gca()); // previous image is deleted
 
 Diagram = gca();
 
 [NumberOfRows NumberOfColumns] = size(Image);
 
 Diagram.data_bounds = [1, 1; NumberOfColumns, NumberOfRows];
 
 Diagram.axes_visible = ['off' 'off' 'off'];
 
 Diagram.isoview = 'on';
 
 Options = '082'; // Box is drawn around image.
 
 select type(Image)
   
  case TYPE_BOOLEAN
    
   Matplot((2 ^ 8 - 1) * bool2s(Image), Options);
   
  case TYPE_INT    
    
   Matplot(Image, Options);
   
  case TYPE_DOUBLE
    
   if max(Image) > 1 then 
      
    Matplot(Image, Options) 
      
   else 
      
    Matplot(round((2 ^ 8 - 1) * Image), Options); 
      
   end;
    
 end;
 drawnow();
endfunction

