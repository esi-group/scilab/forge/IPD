function [CumulatedSizeHistogram, ListOfSizes] = ...
CumulateSizeHistogram(BlobImage)
//////////////////////////////////////////////////////////////////////////// 
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////
 
 [NumberOfOutputs NumberOfInputs] = argn();
 
 if NumberOfInputs ~= 1 then
  
  error('Wrong number of input parameters.');
  
 end;
 
 CheckNumericMatrix(BlobImage, 'BlobImage');
 
 ImageType = typeof(BlobImage);
 
 if (ImageType ~= 'uint32') & (ImageType ~= 'int32') then
   
  error('BlobImage must be a 2D matrix of type uint32 or int32.');
   
 end;
 
 if NumberOfOutputs > 2 then
   
  error('Wrong number of output arguments.');
    
 end;
 
 if NumberOfOutputs == 2 then
   
  [SizeHistogram ListOfSizes] = CreateSizeHistogram(BlobImage);
   
 else
   
  SizeHistogram = CreateSizeHistogram(BlobImage); 
   
 end;

 if isempty(SizeHistogram) then
 
  return;
   
 end;
  
 CumulatedSizeHistogram = cumsum(SizeHistogram) / sum(SizeHistogram);

endfunction
