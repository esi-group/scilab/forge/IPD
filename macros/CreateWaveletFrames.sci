function ListOfImages = CreateWaveletFrames(Image, WaveletType, MaxLevel)
//////////////////////////////////////////////////////////////////////////// 
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////

global TYPE_DOUBLE;

global TYPE_INT;

// Input and output parameters are checked.

[NumberOfOutputs NumberOfInputs] = argn();

if NumberOfInputs ~= 3 then
  
 error('Wrong number of input parameters.');
  
end;

// Image must be a numeric 2D matrix.
CheckNumericMatrix(Image);

select type(Image)
  
 case TYPE_DOUBLE
   
  Approximation = Image;
  
 case TYPE_INT
   
  select typeof(Image)
    
   case 'uint8' 
     
    MaxGrayLevel = 2 ^ 8 - 1; 
    
   case 'uint16' 
     
    MaxGrayLevel = 2 ^ 16 - 1;  
      
   case 'uint32'
     
    MaxGrayLevel = 2 ^ 32 - 1;  
         
  end;
  
  Approximation = double(Image) / MaxGrayLevel;
     
 else
   
  error('Image must be a 2D matrix of type uint8, uint16, ' + ...
        'uint32 or double.');
  
end;
     
// WaveletType must be a scalar of type uint8.

CheckScalar(WaveletType);

if typeof(WaveletType) ~= 'uint8' then

 error('WaveletType must be a scalar of type uint8.');
    
end;

// MaxLevel must be an integer scalar greater than zero.

CheckScalar(MaxLevel);

if (MaxLevel ~= round(MaxLevel)) | (MaxLevel <= 0) then
  
 error('MaxLevel must be an integer number greater than zero.');
 
end;

// There must be exactly one output parameter.

if NumberOfOutputs ~= 1 then
  
 error('Wrong number of output arguments.');
  
end;
      
// The wavelet filters are read.
OriginalFilters = GetWaveletFilters(WaveletType);

// The output parameter is a 3D matrix. This 3D matrix contains the horizon-
// tal, diagonal and vertical detail (in this sequence) for each level. The
// approximation at the highest level is stored in ListOfImages(:, :, $).

[NumberOfRows NumberOfColumns] = size(Image);

ListOfImages = zeros(NumberOfRows, NumberOfColumns, 3 * MaxLevel + 1);

Channel = 1;

for n = 1 : MaxLevel
  
 if n > 1 then
   
  LowPass = UpsampleFilter(LowPass); 
  
  HighPass = UpsampleFilter(HighPass);
     
 else
   
  LowPass = OriginalFilters.LowPass;
   
  HighPass = OriginalFilters.HighPass;
      
 end; 
 
 ListOfImages(:, :, Channel) = SeparableFilter(Approximation, ...
                                               HighPass, ...
                                               LowPass);
                                               
 Channel = Channel + 1;
                                               
 ListOfImages(:, :, Channel) = SeparableFilter(Approximation, ...
                                               HighPass, ...
                                               HighPass);
                                                                    
 Channel = Channel + 1;
 
 ListOfImages(:, :, Channel) = SeparableFilter(Approximation, ...
                                               LowPass, ...
                                               HighPass);
                                               
 Channel = Channel + 1;
 
 if n < MaxLevel then
                                               
  Approximation = SeparableFilter(Approximation, LowPass, LowPass);
                                                 
 else
   
  ListOfImages(:, :, $) = SeparableFilter(Approximation, LowPass, LowPass);  
   
 end;
  
end;

endfunction
