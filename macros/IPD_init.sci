//////////////////////////////////////////////////////////////////////////// 
// IPD - Image Processing Design Toolbox
//
// Copyright (c) Allan CORNET - 2012
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////
function IPD_init()
  global IPD_PATH;
  global TYPE_DOUBLE;
  global TYPE_INT;
  global TYPE_BOOLEAN;
  global TYPE_STRING;
  global TYPE_STRUCT;
  global TYPE_LIST;
  global TYPE_GRAPHIC_HANDLE;
  global TYPE_POINTER;
  global STRUCTURE_ELEMENT_RECT;
  global STRUCTURE_ELEMENT_CROSS; 
  global STRUCTURE_ELEMENT_CIRCLE;
  global STRUCTURE_ELEMENT_CUSTOM; 
  global FILTER_DILATE;
  global FILTER_ERODE;
  global FILTER_CLOSE;
  global FILTER_OPEN;
  global FILTER_TOP_HAT;
  global FILTER_BOTTOM_HAT;
  global CONVERSION_RGB2GRAY;
  global CONVERSION_RGB2LAB;
  global CONVERSION_LAB2RGB;
  global EDGE_SOBEL;
  global EDGE_LAPLACE;
  global EDGE_PREWITT;
  global EDGE_SCHARR;
  global TEXTURE_EDGE_EDGE;
  global TEXTURE_EDGE_LEVEL;
  global TEXTURE_EDGE_RIPPLE;
  global TEXTURE_EDGE_SPOT;
  global TEXTURE_LEVEL_RIPPLE;
  global TEXTURE_LEVEL_SPOT;
  global TEXTURE_RIPPLE_RIPPLE;
  global TEXTURE_RIPPLE_SPOT;
  global TEXTURE_SPOT_SPOT;
  global WAVELET_DAUBECHIES_2;
  global WAVELET_DAUBECHIES_4;
  global WAVELET_DAUBECHIES_6;
  global WAVELET_DAUBECHIES_8;
  global WAVELET_DAUBECHIES_10;
  global WAVELET_DAUBECHIES_12;
  global WAVELET_DAUBECHIES_14;
  global WAVELET_DAUBECHIES_16;
  global WAVELET_DAUBECHIES_18;
  global WAVELET_DAUBECHIES_20;
  global WAVELET_COIFLET_6;
  global WAVELET_COIFLET_12;
  global WAVELET_COIFLET_18;
  global WAVELET_COIFLET_24;
  global WAVELET_COIFLET_30;
  global WAVELET_CDF;
  global WAVELET_PATH;
  global MATCH_LEAST_SQUARES;
  global MATCH_CORRELATION;
  global MATCH_LEAST_SQUARES_NORM;
  global MATCH_CORRELATION_NORM;


  IPD_PATH = IPD_GetRootPath() + "/etc/";
  WAVELET_PATH = IPD_PATH;
  IPD_PATH = IPD_GetRootPath();

  // define constants
  TYPE_DOUBLE = 1;
  TYPE_INT = 8;
  TYPE_BOOLEAN = 4;
  TYPE_STRING = 10;
  TYPE_STRUCT = 17;
  TYPE_LIST = 15;
  TYPE_GRAPHIC_HANDLE = 9;
  TYPE_POINTER = 128;
  STRUCTURE_ELEMENT_RECT = 0; // structure element shape in libIPD.dll 
  STRUCTURE_ELEMENT_CROSS = 1; // structure element shape in libIPD.dll 
  STRUCTURE_ELEMENT_CIRCLE = 2; // structure element shape in libIPD.dll 
  STRUCTURE_ELEMENT_CUSTOM = 3; // structure element shape in libIPD.dll 
  FILTER_DILATE = uint8(0); // filter in libIPD.dll
  FILTER_ERODE = uint8(1); // filter in libIPD.dll
  FILTER_CLOSE = uint8(2); // filter in libIPD.dll
  FILTER_OPEN = uint8(3); // filter in libIPD.dll
  FILTER_TOP_HAT = uint8(4); // filter in libIPD.dll
  FILTER_BOTTOM_HAT = uint8(5); // filter in libIPD.dll
  CONVERSION_RGB2GRAY = uint8(0); // conversion in libIPD.dll
  CONVERSION_RGB2LAB = uint8(1); // conversion in libIPD.dll
  CONVERSION_LAB2RGB = uint8(2); // conversion in libIPD.dll
  EDGE_SOBEL = uint8(0); // edge filter in libIPD.dll
  EDGE_LAPLACE = uint8(1); // edge filter in libIPD.dll
  EDGE_PREWITT = uint8(2); // edge filter in libIPD.dll
  EDGE_SCHARR = uint8(3); // edge filter in libIPD.dll
  TEXTURE_EDGE_EDGE = uint8(0); // texture energy filter
  TEXTURE_EDGE_LEVEL = uint8(1); // texture energy filter
  TEXTURE_EDGE_RIPPLE = uint8(2); // texture energy filter
  TEXTURE_EDGE_SPOT = uint8(3); // texture energy filter
  TEXTURE_LEVEL_RIPPLE = uint8(4); // texture energy filter
  TEXTURE_LEVEL_SPOT = uint8(5); // texture energy filter
  TEXTURE_RIPPLE_RIPPLE = uint8(6); // texture energy filter
  TEXTURE_RIPPLE_SPOT = uint8(7); // texture energy filter
  TEXTURE_SPOT_SPOT = uint8(8); // texture energy filter
  WAVELET_DAUBECHIES_2 = uint8(0); // Daubechies 2 wavelet
  WAVELET_DAUBECHIES_4 = uint8(1); // Daubechies 4 wavelet
  WAVELET_DAUBECHIES_6 = uint8(2); // Daubechies 6 wavelet
  WAVELET_DAUBECHIES_8 = uint8(3); // Daubechies 8 wavelet
  WAVELET_DAUBECHIES_10 = uint8(4); // Daubechies 10 wavelet
  WAVELET_DAUBECHIES_12 = uint8(5); // Daubechies 12 wavelet
  WAVELET_DAUBECHIES_14 = uint8(6); // Daubechies 14 wavelet
  WAVELET_DAUBECHIES_16 = uint8(7); // Daubechies 16 wavelet
  WAVELET_DAUBECHIES_18 = uint8(8); // Daubechies 18 wavelet
  WAVELET_DAUBECHIES_20 = uint8(9); // Daubechies 20 wavelet
  WAVELET_COIFLET_6 = uint8(10); // Coiflet 6
  WAVELET_COIFLET_12 = uint8(11); // Coiflet 12
  WAVELET_COIFLET_18 = uint8(12); // Coiflet 18
  WAVELET_COIFLET_24 = uint8(13); // Coiflet 24
  WAVELET_COIFLET_30 = uint8(14); // Coiflet 30
  WAVELET_CDF = uint8(15); // Cohen Daubechies Feaveau wavelet
  MATCH_LEAST_SQUARES = uint8(16); // template matching by least squares
  MATCH_CORRELATION = uint8(17); // template matching by correlation
  MATCH_LEAST_SQUARES_NORM = uint8(18); // normalized template matching
  MATCH_CORRELATION_NORM = uint8(19); // normalized template matching
 
endfunction