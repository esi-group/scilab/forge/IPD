function FigureHandle = ImageTool(Image, Title)
/////////////////////////////////////////////////////////////////////////
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////


 // Global variables used as constants are declared.

 global TYPE_INT;

 global TYPE_DOUBLE;

 global TYPE_BOOLEAN;

 SCILAB_FUNCTION = 2;

 // Input and output parameters are checked.

 [NumberOfOutputs NumberOfInputs] = argn();

 // There must be exactly one output parameter.

 if NumberOfOutputs ~= 1 then

  error('Wrong number of output parameters.');

 end

 // There must be exactly one input parameter. This parameter must be a
 // 2D or 3D matrix of type uint8, uint16, uint32, double or boolean.

 if (NumberOfInputs < 1) | (NumberOfInputs > 2) then

  error('Wrong number of input parameters.');

 end;

 ImageType = type(Image(1));

 if (ImageType ~= TYPE_INT) ...
  & (ImageType ~= TYPE_DOUBLE) ...
  & (ImageType ~= TYPE_BOOLEAN) then

  error('Image must be a 2D or 3D matrix of type uint8, uint16, ' + ...
        'uint32, boolean or double.');

 end;

 if NumberOfInputs < 2 then 

  Title = '';

 else

  CheckString(Title);
   
 end

 // Image is displayed.

 FigureHandle = figure('figure_name', Title);

 ImageSize = size(Image);

 select length(ImageSize)

  case 2

   ShowImage(Image, Title);

  case 3

   ShowColorImage(Image, Title);

  else

   error('Image must be a 2D or 3D matrix of type uint8, uint16, ' + ...
         'uint32, boolean or double.');

 end;

 ImageArea = FigureHandle.children;

 ImageArea.margins = [0 0 0 0];

 ImageArea.box = 'hidden_axes';
 
 // Properties of Image are stored.

 set(FigureHandle, ...
     'UserData', ...
     struct('Path', ...
            pwd(), ...
            'ImageType', ...
            ImageType, ...
            'ImageSize', ...
            ImageSize, ...
            'Image', ...
            Image, ...
            'SelectedPixel', ...
            [], ...
            'SelectedLine', ...
            [], ...
            'SelectedRectangle', ...
            [], ...
            'StartPointROI', ...
            [], ...
            'EndPointROI', ...
            [], ...
            'StartPointLine', ...
            [], ...
            'EndPointLine', ...
            [], ...
            'ListOfDiagrams', ...
            list()));

 // Menus are created.

 ListOfMenus = list();

 ListOfItems = list();

 ListOfActions = list(list(SCILAB_FUNCTION, 'CallbackImageFile'), ...
                      list(SCILAB_FUNCTION, 'CallbackSelectedPixel'), ...
                      list(SCILAB_FUNCTION, 'CallbackSelectedLine'), ...
                      list(SCILAB_FUNCTION, 'CallbackSelectedROI'), ...
                      list(SCILAB_FUNCTION, 'CallbackHelp'));

 select getlanguage()

  case 'de_DE'

   ListOfMenus(1) = 'Datei';

   ListOfItems(1) = ['Öffnen'; 'Speichern'; 'Schließen'];

   ListOfMenus(2) = 'Pixel';

   ListOfItems(2) = ['Auswählen'; ...
                     'Zeilenprofil'; ...
                     'Zeilenhistogramm'; ...
                     'Spaltenprofil'; ...
                     'Spaltenhistogramm'; ...
                     'Als globale Variable exportieren'; ...
                     'Auswahl aufheben'];

   ListOfMenus(3) = 'Linie';

   ListOfItems(3) = ['Auswählen'; ...
                     'Profil'; ...
                     'Histogramm'; ...
                     'Als globale Variable exportieren'; ...
                     'Auswahl aufheben'];

   ListOfMenus(4) = 'Rechteckiger Bereich';

   ListOfItems(4) = ['Auswählen'; ...
                     'Histogramm'; ...
                     'Als globale Variable exportieren'; ...
                     'Auswahl aufheben'];

   ListOfMenus(5) = '?';

   ListOfItems(5) = ['Scilab'; ...
                     'Image Processing Design Toolbox'; ...
                     'Über Scilab'];

   delmenu(FigureHandle.figure_id, 'File');

   delmenu(FigureHandle.figure_id, 'Tools');
   
   delmenu(FigureHandle.figure_id, 'Edit');
   
   delmenu(FigureHandle.figure_id, '?');

  case 'en_US'

   ListOfMenus(1) = 'File';

   ListOfItems(1) = ['Open'; 'Save'; 'Close'];

   ListOfMenus(2) = 'Pixel';

   ListOfItems(2) = ['Select'; ...
                     'Row Profile'; ...
                     'Row Histogram'; ...
                     'Column Profile'; ...
                     'Column Histogram'; ...
                     'Export as Global Variable'; ...
                     'Cancel Selection'];

   ListOfMenus(3) = 'Line';

   ListOfItems(3) = ['Select'; ...
                     'Profile'; ...
                     'Histogram'; ...
                     'Export as Global Variable'; ...
                     'Cancel Selection'];

   ListOfMenus(4) = 'Rectangular Area';

   ListOfItems(4) = ['Select'; ...
                     'Histogram'; ...
                     'Export as Global Variable'; ...
                     'Cancel Selection'];

   ListOfMenus(5) = '?';

   ListOfItems(5) = ['Scilab'; ...
                     'Image Processing Design Toolbox'; ...
                     'about Scilab'];

   delmenu(FigureHandle.figure_id, 'File');

   delmenu(FigureHandle.figure_id, 'Tools');

   delmenu(FigureHandle.figure_id, 'Edit');

   delmenu(FigureHandle.figure_id, '?');

  case 'ja_JP'

   ListOfMenus(1) = 'ファイル';

   ListOfItems(1) = ['開く'; '蓄積する'; '閉じる'];

   ListOfMenus(2) = '画素';

   ListOfItems(2) = ['選択する'; ...
                     '行におけるプロファイル'; ...
                     '行におけるヒストグラム'; ...
                     '列におけるプロファイル'; ...
                     '列におけるヒストグラム'; ...
                     'グローバル変数としてエキスポートする'; ...
                     '選択をキャンセルする'];

   ListOfMenus(3) = '線';

   ListOfItems(3) = ['選択する'; ...
                     'プロファイル'; ...
                     'ヒストグラム'; ...
                     'グローバル変数としてエキスポートする'; ...
                     '選択をキャンセルする'];

   ListOfMenus(4) = '四角の領域';

   ListOfItems(4) = ['選択する'; ...
                     'ヒストグラム'; ...
                     'グローバル変数としてエキスポートする'; ...
                     '選択をキャンセルする'];

   ListOfMenus(5) = '?';

   ListOfItems(5) = ['Scilabヘルプ'; ...
                     'Image Processing Design Toolboxヘルプ'; ...
                     'Scilabバージョン情報'];

   delmenu(FigureHandle.figure_id, 'ファイル(F)');

   delmenu(FigureHandle.figure_id, 'ツール(T)');

   delmenu(FigureHandle.figure_id, '編集(E)');

   delmenu(FigureHandle.figure_id, 'その他(?)');

  else

   ListOfMenus(1) = 'File';
   
   ListOfItems(1) = ['Open'; 'Save'; 'Close'];
   
   ListOfMenus(2) = 'Selected Pixel';

   ListOfItems(2) = ['Select'; ...
                     'Row Profile'; ...
                     'Row Histogram'; ...
                     'Column Profile'; ...
                     'Column Histogram'; ...
                     'Export as Global Variable'; ...
                     'Cancel Selection'];

   ListOfMenus(3) = 'Line';

   ListOfItems(3) = ['Select'; ...
                     'Profile'; ...
                     'Histogram'; ...
                     'Export as Global Variable'; ...
                     'Cancel Selection'];

   ListOfMenus(4) = 'Rectangular Area';

   ListOfItems(4) = ['Select'; ...
                     'Histogram'; ...
                     'Export as Global Variable'; ...
                     'Cancel Selection'];

   ListOfMenus(5) = '?';

   ListOfItems(5) = ['Scilab'; ...
                     'Image Processing Design Toolbox'; ...
                     'about Scilab'];

   delmenu(FigureHandle.figure_id, 'File');

   delmenu(FigureHandle.figure_id, 'Tools');

   delmenu(FigureHandle.figure_id, 'Edit');

   delmenu(FigureHandle.figure_id, '?');

 end;

 for n = 1 : length(ListOfMenus) 

  addmenu(FigureHandle.figure_id, ...
          ListOfMenus(n), ...
          ListOfItems(n), ...
          ListOfActions(n));

  if (n > 1) & (n < length(ListOfMenus)) then

   for m = 2 : size(ListOfItems(n), 1)

    unsetmenu(FigureHandle.figure_id, ListOfMenus(n), m);

   end;

  end;

 end;

 // Callback for mouse move over image is set.

 set(FigureHandle, 'event_handler', 'CallbackMouseMove');

 set(FigureHandle, 'event_handler_enable', 'on');

endfunction
