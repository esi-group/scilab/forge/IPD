function Image = RGB2Gray(RGB)
//////////////////////////////////////////////////////////////////////////// 
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////

 // Global variables used as constants are declared.
 
 global TYPE_INT;
 
 global CONVERSION_RGB2GRAY;
 
 // Input and output parameters are checked.
 
 [NumberOfOutputs NumberOfInputs] = argn();
 
 if NumberOfInputs ~= 1 then
  
  error('Wrong number of input parameters.');
  
 end;
 
 if ndims(RGB) ~= 3 then
  
  error('Parameter RGB must be a 3D matrix.');
  
 end;
 
 if NumberOfOutputs ~= 1
  
  error('Wrong number of output parameters.');
  
 end;
 
 // If RGB is of type uint8 or double, the conversion can be carried out
 // directly. Otherwise RGB is converted to double and the gray level image
 // is converted back to the data type of RGB.
 
 Sample = RGB(1);
 
 IsInteger = (type(Sample) == TYPE_INT);
 
 Dimensions = size(RGB);
 
 if IsInteger then
 
  select typeof(Sample)
    
   case 'uint8'
     
    PixelList = ConvertColorSpace(RGB(:), size(RGB), CONVERSION_RGB2GRAY);
     
   case 'uint16'  
     
    MaxGrayValue = (2 ^ 16 - 1);
     
    RGBAsDouble = double(RGB) / MaxGrayValue;
     
    PixelListAsDouble = ConvertColorSpace(RGBAsDouble(:), ...
                                          size(RGB), ...
                                          CONVERSION_RGB2GRAY);
      
    PixelList = uint16(ImageAsDouble * MaxGrayValue);
      
   case 'uint32'
     
    MaxGrayValue = (2 ^ 32 - 1); 
     
    RGBAsDouble = double(RGB) / MaxGrayValue;
     
    PixelListAsDouble = ConvertColorSpace(RGBAsDouble(:), ...
                                          size(RGB), ...
                                          CONVERSION_RGB2GRAY);
      
    PixelList = uint32(ImageAsDouble * MaxGrayValue);
 
  end; 

 else
   
  PixelList = ConvertColorSpace(RGB(:), size(RGB), CONVERSION_RGB2GRAY);
    
 end;
 
 Image = matrix(PixelList, Dimensions(1 : 2));
 
endfunction
