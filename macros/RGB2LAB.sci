function LAB = RGB2LAB(RGB)
//////////////////////////////////////////////////////////////////////////// 
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////

 // Global variables used as constants are declared.

 global TYPE_DOUBLE;

 global CONVERSION_RGB2LAB;

 // Input and output parameters are checked.
 
 [NumberOfOutputs NumberOfInputs] = argn();
 
 if NumberOfInputs ~= 1 then
   
  error('Wrong number of input parameters.');
   
 end;
 
 if ndims(RGB) ~= 3 then
   
  error('RGB must be a 3D matrix of type double.');
   
 end;
 
 if NumberOfOutputs ~= 1 then
   
  error('Wrong number of output parameters.');
   
 end;
 
 // Only double images are supported.
 
 if type(RGB(1)) == TYPE_DOUBLE then
   
  PixelList = ConvertColorSpace(RGB(:), size(RGB), CONVERSION_RGB2LAB);
   
  LAB = matrix(PixelList, size(RGB));  
  
 else
   
  error('RGB must be of type double.');
  
 end;   

endfunction
