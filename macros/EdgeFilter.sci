function GradientImage = EdgeFilter(Image, FilterType)
//////////////////////////////////////////////////////////////////////////// 
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////

// Global variables used as constants are declared.

global FILTER_SOBEL;

global FILTER_PREWITT;

global FILTER_LAPLACE;

global FILTER_SCHARR;

global TYPE_DOUBLE;

global TYPE_INT;

// Input and output parameters are checked.

[NumberOfOutputs NumberOfInputs] = argn();

if NumberOfInputs ~= 2 then
  
 error('Wrong number of input parameters.');
  
end;

CheckNumericMatrix(Image, 'Image');

if (length(FilterType) ~= 1) | (typeof(FilterType) ~= 'uint8') then
  
 error('FilterType must be a scalar of type uint8.');
  
end;

if NumberOfOutputs ~= 1 then
  
 error('Wrong number of output parameters.');
  
end;

// If Image is not of type double, it is converted to double.

ImageType = type(Image);

select ImageType
  
 case TYPE_DOUBLE
  
  ImageAsDouble = Image;
  
 case TYPE_INT
  
  select typeof(Image)
    
   case 'uint8'
     
    MaxGrayValue = 2 ^ 8 - 1; 
      
   case 'uint16'
     
    MaxGrayValue = 2 ^ 16 - 1; 
        
   case 'uint32'
     
    MaxGrayValue = 2 ^ 32 - 1; 
          
   else
     
    error('Image must be of type uint8, uint16, uint32 or double.');
        
  end;
  
  ImageAsDouble = double(Image) / MaxGrayValue;
  
end;

// If FilterType corresponds to a separable filter, the row and column 
// filters are created. If the filter to be applied is not separable, a
// rectangular mask is created.

select FilterType
  
 case EDGE_SOBEL
   
  ColumnFilter = [1; 2; 1]; 
  
  RowFilter = [-1, 0, 1];
  
  IsSeparable = %t;
   
 case EDGE_PREWITT 
   
  ColumnFilter = [1; 1; 1]; 
  
  RowFilter = [-1, 0, 1];
  
  IsSeparable = %t; 
    
 case EDGE_LAPLACE
   
  Mask = [1, 1, 1; 1, -8, 1; 1, 1, 1]; 
  
  IsSeparable = %f;
   
 case EDGE_SCHARR
   
  ColumnFilter = [3; 10; 3]; 
  
  RowFilter = [-1, 0, 1];
  
  IsSeparable = %t;
  
 else
   
  error('Unknown edge filter.');
     
end;

// Filter is applied now.

if IsSeparable then
  
 FirstPass = SeparableFilter(ImageAsDouble, ColumnFilter, RowFilter);
  
 SecondPass = SeparableFilter(ImageAsDouble, RowFilter, ColumnFilter); 
 
 FilteredImage = abs(FirstPass) + abs(SecondPass);
   
else
  
 FilteredImage = abs(MaskFilter(ImageAsDouble, Mask)); 
  
end;

// GradientImage is of the same type as Image.

select ImageType
  
 case TYPE_DOUBLE
  
  GradientImage = FilteredImage;
  
 case TYPE_INT
   
  FilteredImage = round(MaxGrayValue * FilteredImage);
   
  select typeof(Image)
    
   case 'uint8'
     
    GradientImage = uint8(FilteredImage); 
      
   case 'uint16'
     
    GradientImage = uint16(FilteredImage); 
        
   case 'uint32'
     
    GradientImage = uint32(FilteredImage); 
     
  end;
   
end;

endfunction
