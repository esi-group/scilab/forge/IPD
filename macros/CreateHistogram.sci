function [Histogram, ListOfBins] = CreateHistogram(Image, NumberOfBins)
//////////////////////////////////////////////////////////////////////////// 
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////

 // Global variables used as constants are declared.
 
 global TYPE_INT;
 
 global TYPE_DOUBLE;
 
 global TYPE_BOOLEAN;
 
 // Input and output arguments are checked.
 
 [NumberOfOutputs NumberOfInputs] = argn();
 
 if (NumberOfInputs < 1) | (NumberOfInputs > 2) then
  
  error('Wrong number of input parameters.');
  
 end;
 
 CheckMatrix(Image, 'Image');
 
 if (NumberOfOutputs < 1) | (NumberOfOutputs > 2)
  
  error('Wrong number of output parameters.');
  
 end;
 
 // The histogram calculation method depends on the data type of Image.
 
 select type(Image)
  
  case TYPE_BOOLEAN
   
   // The first histogram component contains the number of false pixels,
   // the second components contains the number of true pixels.
   
   Histogram = zeros(1, 2);
   
   Histogram(2) = length(find(Image));
   
   Histogram(1) = length(Image) - Histogram(2);
   
   if NumberOfOutputs == 2 then
    
    ListOfBins = [0 1];
    
   end;
      
  case TYPE_INT
   
   // For a uint8 type image the histogram contains 256 entries, for a uint16 
   // or uint32 type image there are as many entries as gray values.
   
   if typeof(Image) == 'uint8' then
     
    MaxGrayValue = 255;
     
   else
     
    MaxGrayValue = max(Image);
     
   end;
  
   ListOfGrayValues = double(0 : MaxGrayValue);
   
   [IndexMatrix Histogram] = dsearch(double(Image), ListOfGrayValues, 'd');
   
   if NumberOfOutputs == 2 then
    
    ListOfBins = ListOfGrayValues;
    
   end;
          
  case TYPE_DOUBLE
   
   // The number of bins is relevant only for double type images. 
      
   ListOfGrayValues = unique(Image);
   
   NumberOfGrayValues = length(ListOfGrayValues);
   
   if NumberOfInputs == 2 then
   
    // NumberOfBins is specified so the gray values are grouped.
    
    CheckScalar(NumberOfBins);
    
    if (NumberOfBins ~= round(NumberOfBins)) | (NumberOfBins < 1) then
      
     error('NumberOfBins must be an integer number greater than zero.');
  
    end;
   
    Step = 1 / NumberOfBins;
    
    ListOfIntervals = 0 : Step : 1;
    
    [IndexMatrix Histogram] = dsearch(Image, ListOfIntervals, 'c');
        
    if NumberOfOutputs == 2 then
    
     ListOfBins = ListOfIntervals(2 : $) - 0.5 * Step;
    
    end;
   
   else
   
    // NumberOfBins is not specified so for each gray value the frequence is
    // calculated.
    
    [IndexMatrix Histogram] = dsearch(Image, ListOfGrayValues, 'd');
   
    if NumberOfOutputs == 2 then
    
     ListOfBins = ListOfGrayValues;
    
    end;
          
   end;
     
 end;
    
endfunction

