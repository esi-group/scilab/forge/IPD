///////////////////////////////////////////////////////////////////////
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
// Allan CORNET - 2012
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
///////////////////////////////////////////////////////////////////////
function subdemolist = demo_gateway()
  demopath = get_absolute_file_path("IPD.dem.gateway.sce");

  select getlanguage()
    case 'de_DE'
      SubDemoTitles = ['Blobanalyse'; ..
                   'Kantendetektion'; ..
                   'Morphologische Filterung'; ..
                   'Template Matching'; ..
                   'Textur-Segmentierung'; ..
                   'Video anzeigen'; ..
                   'Wasserscheidentransformation'; ..
                   'Waveletframes'];
    case 'en_US'
      SubDemoTitles = ['Blob analysis'; ..
                    'Edge detection'; ..
                    'Morphological filtering'; ..
                    'Template matching'; ..
                    'Texture segmentation'; ..
                    'Video display'; ..
                    'Watershed transform'; ..
                    'Wavelet frames'];

    case 'ja_JP'
      SubDemoTitles = ['ブロブの解析'; ..
                    'エッジを抽出する'; ..
                    '形態学のフィルター'; ..
                    'テンプレート・マチング'; ..
                    'テクスチャを用いた分割'; ..
                    'ビデオを表示する'; ..
                    'ウォーターシェッド変換'; ..
                    'ウェブレット・フレーム'];
    else
      SubDemoTitles = [_('Blob analysis'); ..
                   _('Edge detection'); ..
                   _('Morphological filtering'); ..
                   _('Template matching'); ..
                   _('Texture segmentation'); ..
                   _('Video display'); ..
                   _('Watershed transform'); ..
                   _('Wavelet frames')];
  end

  SubDemoFiles = ['BlobAnalysis.sce'; ..
                'EdgeDetection.sce'; ..
                'Morphology.sce'; ..
                'TemplateMatching.sce'; ..
                'TextureSegmentation.sce'; ..
                'VideoDisplay.sce'; ..
                'Watershed.sce'; ..
                'WaveletFrames.sce'];

  subdemolist = cat(2, SubDemoTitles, demopath + SubDemoFiles);
endfunction
///////////////////////////////////////////////////////////////////////
subdemolist = demo_gateway();
clear demo_gateway; // remove demo_gateway on stack
///////////////////////////////////////////////////////////////////////
