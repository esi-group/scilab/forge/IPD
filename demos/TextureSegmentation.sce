////////////////////////////////////////////////////////////////////////////
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
// Allan CORNET - 2012
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////
function TextureSegment_demo()

  global TEXTURE_EDGE_LEVEL;
  DemoPath = get_absolute_file_path('TextureSegmentation.sce');
  RGB = ReadImage(DemoPath + 'teaset.png');
  Image = RGB2Gray(RGB);
  TextureImage = CalculateTextureEnergy(Image, TEXTURE_EDGE_LEVEL);
  ThresholdImage = SegmentByThreshold(TextureImage, 100);

  scf();
  ShowImage(TextureImage, 'Texture Image', jetcolormap(fix(max(TextureImage))));

  scf();
  ShowImage(ThresholdImage, 'Segmented Texture Image');

  demo_viewCode('TextureSegmentation.sce');
endfunction
////////////////////////////////////////////////////////////////////////////
TextureSegment_demo();
clear TextureSegment_demo;
////////////////////////////////////////////////////////////////////////////
