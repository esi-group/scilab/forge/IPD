////////////////////////////////////////////////////////////////////////////
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
// Allan CORNET - 2012
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////
function VideoDisplay_demo()

  DemoPath = get_absolute_file_path('VideoDisplay.sce');
  VideoPath = DemoPath + 'red-car-video.avi';
  VideoInfo = GetVideoStruct(VideoPath);
  VideoFilePointer = OpenVideoFile(VideoPath);

  scf();
  for n = 1 : VideoInfo.NumberOfFrames
    RGB = ReadImage(VideoFilePointer);
    ShowColorImage(RGB, VideoPath);
  end;

  CloseVideoFile(VideoFilePointer);
  demo_viewCode('VideoDisplay.sce');

endfunction
////////////////////////////////////////////////////////////////////////////
VideoDisplay_demo();
clear VideoDisplay_demo;
////////////////////////////////////////////////////////////////////////////
