<?xml version="1.0" encoding="UTF-8"?>
<refentry version="5.0-subset Scilab" xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:xi="http://www.w3.org/2001/XInclude"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:html="http://www.w3.org/1999/xhtml"
          xmlns:db="http://docbook.org/ns/docbook">
  <refnamediv xml:id="OpenImage">
    <refname>OpenImage</refname>

    <refpurpose>applies a morphological opening filter to an
    image</refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Calling Sequence</title>

    <synopsis>ResultImage = OpenImage(Image, StructureElement);</synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Parameters</title>

    <variablelist>
      <varlistentry>
        <term>Image</term>

        <listitem>
          <para>2D matrix of type uint8, uint16, uint32, double or
          boolean</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>StructureElement</term>

        <listitem>
          <para>struct with the components Width, Height and Data, should be
          generated using CreateStructureElement</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>ResultImage</term>

        <listitem>
          <para>matrix of the same size and type as Image</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>

    <para>This function applies a morphological opening filter to an image.
    This filter retains dark objects and removes light objects the structuring
    element does not fit in.</para>
  </refsection>

  <refsection>
    <title>Examples</title>

    <programlisting role="example">Image = 0.5 * ones(9, 9) // generate gray image

Image(:, 3) = 0 // draw dark line

Image(:, 7) = 1 // draw light line

StructureElement = CreateStructureElement('square', 3) // generate structuring element

ResultImage = OpenImage(Image, StructureElement)</programlisting>
  </refsection>

  <refsection>
    <title>See also</title>

    <simplelist type="inline">
      <member><link linkend="DilateImage">DilateImage</link></member>

      <member><link linkend="ErodeImage">ErodeImage</link></member>

      <member><link linkend="CloseImage">CloseImage</link></member>

      <member><link linkend="TopHat">TopHat</link></member>

      <member><link linkend="BottomHat">BottomHat</link></member>

      <member><link
      linkend="CreateStructureElement">CreateStructureElement</link></member>

      <member><link
      linkend="MorphologicalFilter">MorphologicalFilter</link></member>
    </simplelist>
  </refsection>
</refentry>
