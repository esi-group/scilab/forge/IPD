<?xml version="1.0" encoding="UTF-8"?>
<refentry version="5.0-subset Scilab" xmlns="http://docbook.org/ns/docbook"
          xmlns:xlink="http://www.w3.org/1999/xlink"
          xmlns:xi="http://www.w3.org/2001/XInclude"
          xmlns:svg="http://www.w3.org/2000/svg"
          xmlns:mml="http://www.w3.org/1998/Math/MathML"
          xmlns:html="http://www.w3.org/1999/xhtml"
          xmlns:db="http://docbook.org/ns/docbook">
  <refnamediv xml:id="WriteImage">
    <refname>WriteImage</refname>

    <refpurpose>saves an image file</refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Calling Sequence</title>

    <synopsis>WriteImage(Image, FilePath);</synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Parameters</title>

    <variablelist>
      <varlistentry>
        <term>Image</term>

        <listitem>
          <para>2D or 3D matrix of type uint8, uint16, uint32, double or
          boolean</para>
        </listitem>
      </varlistentry>

      <varlistentry>
        <term>ImagePath</term>

        <listitem>
          <para>name or complete path of the image file</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>

    <para>This function saves an image as a file. The image is saved as a
    three or single channel image of type uint8.</para>

    <para>If Image is of type double or boolean, the gray or color values are
    scaled to the interval 0 - 255.</para>

    <para>If image is of type uint16 or uint32 and the maximum gray or color
    value is below or equal to 255, the gray values are not modified. If the
    maximum gray value is greater than 255, the gray values greater than zero
    are scaled to the interval 1 - 255 and zero gray or color values stay
    zero.</para>
  </refsection>

  <refsection>
    <title>Examples</title>

    <programlisting role="example">global IPD_PATH;

OriginalImage = ReadImage(IPD_PATH + 'demos\teaset.png'); // image is loaded

WriteImage(OriginalImage, IPD_PATH + 'demos\copy.png'); // copy of image is saved

CopiedImage = ReadImage(IPD_PATH + 'demos\copy.png'); // copy is read

figure(); ShowColorImage(OriginalImage, 'Original'); // original image is displayed

figure(); ShowColorImage(CopiedImage, 'Copy'); // copy is displayed</programlisting>
  </refsection>

  <refsection>
    <title>See also</title>

    <simplelist type="inline">
      <member><link linkend="ReadImage">ReadImage</link></member>

      <member><link linkend="ReadImageFile">ReadImageFile</link></member>

      <member><link linkend="WriteImageFile">WriteImageFile</link></member>
    </simplelist>
  </refsection>
</refentry>
