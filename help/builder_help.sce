//////////////////////////////////////////////////////////////////////////// 
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
// Allan CORNET - 2012
// Harald Galda - 2012
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////

Language = getlanguage();

setlanguage('de_DE');

tbx_builder_help_lang("de_DE", get_absolute_file_path("builder_help.sce"));
                      
setlanguage('en_US');

tbx_builder_help_lang("en_US", get_absolute_file_path("builder_help.sce"));
                      
setlanguage('ja_JP');

tbx_builder_help_lang("ja_JP", get_absolute_file_path("builder_help.sce"));

setlanguage(Language);


