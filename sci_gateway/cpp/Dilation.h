////////////////////////////////////////////////////////////////////////////
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////


#ifndef DILATION_H
#define DILATION_H


#include "MorphologicalFilterTemplate.h"


template<class GrayValueType>
class CDilation : public CMorphologicalFilter<GrayValueType>
{
public:

    CDilation(TUInt NumberOfColumns, TUInt NumberOfRows, const int* const Values):
      CMorphologicalFilter<GrayValueType>(NumberOfColumns, NumberOfRows, Values)
      {
      }

      virtual ~CDilation(){};

protected:
    typedef vector<GrayValueType> TGrayValueList;
    virtual GrayValueType ComputeGrayValue(const TGrayValueList& GrayValues) const;
}; // CDilation


template<class GrayValueType>
GrayValueType CDilation<GrayValueType>::ComputeGrayValue(const TGrayValueList& GrayValues) const
{
    return *max_element(GrayValues.begin(), GrayValues.end());
} // CDilation::ComputeGrayValue


#endif
