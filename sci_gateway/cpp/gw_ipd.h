//////////////////////////////////////////////////////////////////////////// 
// IPD - Image Processing Design Toolbox
//
// Copyright (c) Allan CORNET - 2012
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////


#ifndef __GW_IPD_H__
#define __GW_IPD_H__

#ifdef __cplusplus
extern "C" {
#endif

    int sci_CloseVideoFile(char *fname);
    int sci_ConvertColorSpace(char *fname);
    int sci_CreatePixelIndexList(char *fname);
    int sci_DistanceTransform(char *fname);
    int sci_GetVideoInfo(char *fname);
    int sci_MaskFilter(char *fname);
    int sci_MatchTemplate(char *fname);
    int sci_MedianFilter(char *fname);
    int sci_MorphologicalFilter(char *fname);
    int sci_OpenVideoFile(char *fname);
    int sci_ReadImageFile(char *fname);
    int sci_ReadImageFromVideo(char *fname);
    int sci_SearchBlobs(char *fname);
    int sci_SeparableFilter(char *fname);
    int sci_VarianceFilter(char *fname);
    int sci_Watershed(char *fname);
    int sci_WriteImageFile(char *fname);

#ifdef __cplusplus
};
#endif

#endif /* __GW_IPD_H__ */
