//////////////////////////////////////////////////////////////////////////// 
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////

#ifndef FILTER_H
#define FILTER_H

#include "IPD.h"


template<class GrayValueType>
class CFilter
{
public:

    CFilter(TUInt NumberOfColumns, TUInt NumberOfRows);

    virtual ~CFilter(){};

    void FilterImage(TUInt ImageWidth, 
        TUInt ImageHeight, 
        const GrayValueType* const Source, 
        GrayValueType* Destination) const;

protected:

    typedef vector<GrayValueType> TGrayValueList;

    typedef vector<int> TOffsetList;

    virtual GrayValueType ComputeGrayValue(const TGrayValueList& GrayValues) const = 0;

    TUInt m_NumberOfElements;

    TOffsetList m_ColumnOffsets;

    TOffsetList m_RowOffsets;
}; // CFilter


template<class GrayValueType>
CFilter<GrayValueType>::CFilter(TUInt NumberOfColumns, TUInt NumberOfRows):
m_NumberOfElements(0),
    m_ColumnOffsets(TOffsetList(0)),
    m_RowOffsets(TOffsetList(0))
{
} // CFilter::CFilter


template<class GrayValueType>
void CFilter<GrayValueType>::FilterImage(TUInt ImageWidth, 
    TUInt ImageHeight, 
    const GrayValueType* const Source, 
    GrayValueType* Destination) const
{
    // If Source and Destination point to the same address, a different array must be alloca-
    // ted. If this fails, the function has no effect.

    bool InPlace = (Source == Destination);

    GrayValueType* Temp = NULL;

    if(InPlace)
    {
        Temp = new GrayValueType[ImageWidth * ImageHeight];

        if(Temp == NULL)
        {
            return;
        } // if
    } // if

    TUInt PixelIndex = 0;

    TGrayValueList GrayValues(m_NumberOfElements);

    for(TUInt Column = 0; Column < ImageWidth; Column++)
    {
        for(TUInt Row = 0; Row < ImageHeight; Row++)
        {
            // For the current pixel a list of gray values at its neighbor pixels is calculated. A
            // gray value is selected or computed from this list and stored in Destination.

            for(TUInt Index = 0; Index < m_NumberOfElements; Index++)
            {
                TUInt NeighborColumn = 0;

                int NewColumn = Column + m_ColumnOffsets[Index];

                if(NewColumn >= 0)
                {
                    NeighborColumn = static_cast<TUInt>(NewColumn);

                    if(NeighborColumn >= ImageWidth)
                    {
                        NeighborColumn = ImageWidth - 1 - (NeighborColumn - ImageWidth);
                    }// if
                }
                else
                {
                    NeighborColumn = -NewColumn - 1;
                }// if

                TUInt NeighborRow = 0;

                int NewRow = Row + m_RowOffsets[Index];

                if(NewRow >= 0)
                {
                    NeighborRow = static_cast<TUInt>(NewRow);

                    if(NeighborRow >= ImageHeight)
                    {
                        NeighborRow = ImageHeight - 1 - (NeighborRow - ImageHeight);
                    }// if
                }
                else
                {
                    NeighborRow = -NewRow - 1;
                }// if

                GrayValues[Index] = Source[NeighborColumn * ImageHeight + NeighborRow];
            } // for

            if(InPlace)
            {
                Temp[PixelIndex] = ComputeGrayValue(GrayValues);
            }
            else
            {
                Destination[PixelIndex] = ComputeGrayValue(GrayValues);
            } // if

            PixelIndex++;
        } // for
    } // for

    // If Source and Destination point to the same array, Temp is copied to Destination and 
    // Temp is released.
    if(InPlace)
    {
        memcpy(Destination, Temp, ImageWidth * ImageHeight * sizeof(GrayValueType));

        delete Temp;
    } // if
} // CFilter::FilterImage


#endif
