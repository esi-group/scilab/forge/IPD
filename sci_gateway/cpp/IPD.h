//////////////////////////////////////////////////////////////////////////// 
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////

#ifndef IPD_H
#define IPD_H

#include <vector>
#include <list>
#include <memory>
#include <string>
#include <algorithm>
#include <numeric>
#include "cxcore.h"
#include "cv.h"
#include "highgui.h"
using namespace std;

// type definitions

typedef unsigned int TUInt;

typedef unsigned short TUShort;

typedef unsigned char TUChar;

typedef vector<TUInt> TVector;

typedef list<TUInt> TList;

// constants

const TUChar FILTER_DILATE = 0;

const TUChar FILTER_ERODE = 1;

const TUChar FILTER_CLOSE = 2;

const TUChar FILTER_OPEN = 3;

const TUChar FILTER_TOP_HAT = 4;

const TUChar FILTER_BOTTOM_HAT = 5;

const TUChar COLOR_SPACE_GRAY = 0;

const TUChar COLOR_SPACE_RGB = 1;

const TUChar COLOR_SPACE_LAB = 2;

const TUChar RGB2GRAY = 0;

const TUChar RGB2LAB = 1;

const TUChar LAB2RGB = 2;

const int FLOAT_IMAGE = 18; // not used by sci_types in stack-c.h

const TUChar EDGE_SOBEL = 0;

const TUChar EDGE_LAPLACE = 1;

const TUChar EDGE_PREWITT = 2;

const TUChar EDGE_SCHARR = 3;

const TUChar CODEC_LENGTH = 4;

const TUChar MATCH_LEAST_SQUARES = 16;

const TUChar MATCH_CORRELATION = 17;

const TUChar MATCH_LEAST_SQUARES_NORM = 18;

const TUChar MATCH_CORRELATION_NORM = 19;

const TUInt ERROR_FILE_NAME = 38;

const TUInt ERROR_MEMORY = 133;

const TUInt ERROR_FILE_FORMAT = 67;

const TUInt ERROR_SCALAR = 84;

const TUInt ERROR_ARGUMENT = 36;

#endif
