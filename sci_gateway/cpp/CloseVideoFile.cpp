////////////////////////////////////////////////////////////////////////////
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
// Allan CORNET - 2012
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////

#include "gw_ipd.h"
#include "IPD.h"
extern "C"
{
#include "api_scilab.h"
#if API_SCILAB_VERSION < 3
#include "stack-c.h"
#endif
#include "Scierror.h"
};


int sci_CloseVideoFile(char* fname)
{
    // Input and output parameters are checked.

    CheckRhs(1, 1);

    CheckLhs(1, 1);

    // The input argument is a pointer.

    int* Address = NULL;

    SciErr ScilabError = getVarAddressFromPosition(pvApiCtx, 1, &Address);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    void* Pointer = NULL;

    ScilabError = getPointer(pvApiCtx, Address, &Pointer);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    // The pointer is casted to a pointer of type CvCapture* and released.

    CvCapture* VideoFile = static_cast<CvCapture*>(Pointer);

    cvReleaseCapture(&VideoFile);

    // An interface must have at least one output argument. The video file pointer is used for
    // this purpose. The same is done in the corresponding Matlab function.

    ScilabError = createPointer(pvApiCtx, Rhs + 1, (void*) VideoFile);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    // The output argument position is set.
    LhsVar(1) = Rhs + 1;

    return 0;
} // sci_CloseVideoFile
