////////////////////////////////////////////////////////////////////////////
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
// Allan CORNET - 2012
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////

#include "gw_ipd.h"
#include "IPD.h"
#include "LinearFilter.h"
extern "C"
{
#include "api_scilab.h"
#if API_SCILAB_VERSION < 3
#include "stack-c.h"
#endif
#include "Scierror.h"
};



int sci_MaskFilter(char* fname)
{
    // Input and output parameters are checked.

    CheckRhs(2, 2);

    CheckLhs(1, 1);

    // The first argument is a single channel image of type double.

    int* Address = NULL;

    SciErr ScilabError = getVarAddressFromPosition(pvApiCtx, 1, &Address);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    int NumberOfRows = 0;

    int NumberOfColumns = 0;

    double* ImageData = NULL;

    ScilabError = getMatrixOfDouble(pvApiCtx,
        Address,
        &NumberOfRows,
        &NumberOfColumns,
        &ImageData);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    int NumberOfImageRows = NumberOfRows;

    int NumberOfImageColumns = NumberOfColumns;

    // The second input parameter must be a vector of type double.

    ScilabError = getVarAddressFromPosition(pvApiCtx, 2, &Address);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    double* Mask = NULL;

    ScilabError = getMatrixOfDouble(pvApiCtx,
        Address,
        &NumberOfColumns,
        &NumberOfRows,
        &Mask);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    CLinearFilter<double> Filter(NumberOfColumns, NumberOfRows, Mask);

    // Image is convolved with Mask.

    TUInt NumberOfPixels = NumberOfImageRows * NumberOfImageColumns;

    double* FilteredImage = new double[NumberOfPixels];

    if(FilteredImage == NULL)
    {
        Scierror(ERROR_MEMORY, "Internal error occured.");

        return 0;
    } // if

    Filter.FilterImage(NumberOfImageColumns,
        NumberOfImageRows,
        ImageData,
        FilteredImage);

    // The output parameter is created.

    ScilabError = createMatrixOfDouble(pvApiCtx,
        Rhs + 1,
        NumberOfImageRows,
        NumberOfImageColumns,
        FilteredImage);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    // The output parameter position is set.
    LhsVar(1) = Rhs + 1;

    return 0;
} // sci_SeparableFilter
