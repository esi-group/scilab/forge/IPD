////////////////////////////////////////////////////////////////////////////
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
// Allan CORNET - 2012
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////
#include "gw_ipd.h"
#include "IPD.h"
#include "IplImageToScilab.h"
extern "C"
{
#include "api_scilab.h"
#if API_SCILAB_VERSION < 3
#include "stack-c.h"
#endif
#include "Scierror.h"
};
////////////////////////////////////////////////////////////////////////////
int sci_ReadImageFromVideo(char* fname)
{
    // Input and output parameters are checked.

    CheckRhs(1, 1);

    CheckLhs(2, 2);

    // The input parameter is a pointer to the video file.

    int* Address = NULL;

    SciErr ScilabError = getVarAddressFromPosition(pvApiCtx, 1, &Address);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    void* Pointer = NULL;

    ScilabError = getPointer(pvApiCtx, Address, &Pointer);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    CvCapture* VideoFile = static_cast<CvCapture*>(Pointer);

    // The image is read now. If this fails, an empty image is created.

    IplImage* Image = cvQueryFrame(VideoFile);

    if(Image == NULL)
    {
        Image = cvCreateImage(cvSize(0, 0), IPL_DEPTH_8U, 1);

        if(Image == NULL)
        {
            Scierror(ERROR_MEMORY, "Internal error occured.");

            return 0;
        } // if
    } // if

    // The image type is determined and an output variables are created. IPD toolbox handles
    // only unsigned integer and double valued images, but OpenCV does not implement uint32
    // of Scilab. Therefore, images of type uint32 are stored as images of type int32.

    TUInt ImageSize = Image->width * Image->height * Image->nChannels;

    TUChar ColorSpace = COLOR_SPACE_RGB;

    if(Image->nChannels == 1)
    {
        ColorSpace = COLOR_SPACE_GRAY;
    } // if

    switch(Image->depth)
    {
    case IPL_DEPTH_8U:
        {
            TUChar* ImageData = NULL;

            IplImageToScilab(Image, &ImageData, ColorSpace);

            if(ImageData == NULL)
            {
                Scierror(ERROR_MEMORY, "Internal error occured.");

                return 0;
            } // if

            // The first output parameter is a column vector containing the list of intensity values.

            ScilabError = createMatrixOfUnsignedInteger8(pvApiCtx, Rhs + 1, ImageSize, 1, ImageData);

            if(ScilabError.iErr)
            {
                printError(&ScilabError, 0);

                delete ImageData;

                return 0;
            } // if
        } // case

        break;

    case IPL_DEPTH_16U:
        {
            TUShort* ImageData = NULL;

            IplImageToScilab(Image, &ImageData, ColorSpace);

            if(ImageData == NULL)
            {
                Scierror(ERROR_MEMORY, "Internal error occured.");

                return 0;
            } // if

            // The first output parameter is a column vector containing the list of intensity values.

            ScilabError = createMatrixOfUnsignedInteger16(pvApiCtx, Rhs + 1, ImageSize, 1, ImageData);

            if(ScilabError.iErr)
            {
                printError(&ScilabError, 0);

                delete ImageData;

                return 0;
            } // if
        } // case

        break;

    case IPL_DEPTH_32S:
        {
            TUInt* ImageData = NULL;

            IplImageToScilab(Image, &ImageData, ColorSpace);

            if(ImageData == NULL)
            {
                Scierror(ERROR_MEMORY, "Internal error occured.");

                return 0;
            } // if

            // The first output parameter is a column vector containing the list of intensity values.

            ScilabError = createMatrixOfUnsignedInteger32(pvApiCtx, Rhs + 1, ImageSize, 1, ImageData);

            if(ScilabError.iErr)
            {
                printError(&ScilabError, 0);

                delete ImageData;

                return 0;
            } // if
        } // case

        break;

    case IPL_DEPTH_64F:
        {
            double* ImageData = NULL;

            IplImageToScilab(Image, &ImageData, ColorSpace);

            if(ImageData == NULL)
            {
                Scierror(ERROR_MEMORY, "Internal error occured.");

                return 0;
            } // if

            // The first output parameter is a column vector containing the list of intensity values.

            ScilabError = createMatrixOfDouble(pvApiCtx, Rhs + 1, ImageSize, 1, ImageData);

            if(ScilabError.iErr)
            {
                printError(&ScilabError, 0);

                delete ImageData;

                return 0;
            } // if
        } // case

        break;

    default:
        {
            Scierror(ERROR_FILE_FORMAT, "File format can not be processed by IPD toolbox.");

            return 0;
        } // default

        break;
    } // switch

    // The second output parameter is a column vector containing the numbers of rows, columns
    // and channels. The Scilab image is the transposed version of the OpenCV image.

    double Dimensions[3];

    Dimensions[0] = Image->height;

    Dimensions[1] = Image->width;

    Dimensions[2] = Image->nChannels;

    ScilabError = createMatrixOfDouble(pvApiCtx, Rhs + 2, 3, 1, Dimensions);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    // The positions of left hand side variables are set.

    LhsVar(1) = Rhs + 1;

    LhsVar(2) = Rhs + 2;

    return 0;
} // sci_ReadImageFromVideo
