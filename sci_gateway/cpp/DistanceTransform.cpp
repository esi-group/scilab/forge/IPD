////////////////////////////////////////////////////////////////////////////
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
// Allan CORNET - 2012
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////

#include "gw_ipd.h"
#include "IPD.h"
#include "ScilabToIplImage.h"
#include "IplImageToScilab.h"
#include "TransferImageData.h"
extern "C"
{
#include "api_scilab.h"
#if API_SCILAB_VERSION < 3
#include "stack-c.h"
#endif
#include "Scierror.h"
};



int sci_DistanceTransform(char* fname)
{
    // Input and output parameters are checked.

    CheckRhs(1, 1);

    CheckLhs(1, 1);

    // The input parameter is a matrix of type boolean.

    int* ImageAddress = NULL;

    SciErr ScilabError = getVarAddressFromPosition(pvApiCtx, 1, &ImageAddress);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    int NumberOfRows = 0;

    int NumberOfColumns = 0;

    int* ImageData = NULL;

    ScilabError = getMatrixOfBoolean(pvApiCtx,
        ImageAddress,
        &NumberOfRows,
        &NumberOfColumns,
        &ImageData);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    // The image is transformed to an OpenCV image and the distance transform is carried out.
    // The input image is of type boolean whereas cvDistTransform requires a source image of
    // type TUChar/ uint8.

    int NumberOfPixels = NumberOfColumns * NumberOfRows;

    TUChar* ImageDataAsUInt8 = NULL;

    TransferImageData(NumberOfPixels, ImageData, &ImageDataAsUInt8);

    if(ImageDataAsUInt8 == NULL)
    {
        Scierror(ERROR_MEMORY, "Internal error occured.");

        return 0;
    } // if

    IplImage* Source = NULL;

    ScilabToIplImage(&Source,
        ImageDataAsUInt8,
        NumberOfColumns,
        NumberOfRows,
        1,
        sci_ints,
        SCI_UINT8);

    delete ImageDataAsUInt8;

    if(Source == NULL)
    {
        Scierror(ERROR_MEMORY, "Internal error occured.");

        return 0;
    } // if

    IplImage* Destination = cvCreateImage(cvSize(NumberOfColumns, NumberOfRows), IPL_DEPTH_8U, 1);

    if(Destination == NULL)
    {
        Scierror(ERROR_MEMORY, "Internal error occured.");

        cvReleaseImage(&Source);

        return 0;
    } // if

    cvDistTransform(Source, Destination, CV_DIST_L1);

    // The output argument is a matrix of type double.

    TUChar* ResultImageData = NULL;

    IplImageToScilab(Destination, &ResultImageData, COLOR_SPACE_GRAY);

    cvReleaseImage(&Source);

    cvReleaseImage(&Destination);

    ScilabError = createMatrixOfUnsignedInteger8(pvApiCtx,
        Rhs + 1,
        NumberOfRows,
        NumberOfColumns,
        ResultImageData);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        delete ResultImageData;

        return 0;
    } // if

    // Output argument position is set.
    LhsVar(1) = Rhs + 1;

    return 0;
} // sci_DistanceTransform
