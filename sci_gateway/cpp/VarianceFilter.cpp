////////////////////////////////////////////////////////////////////////////
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
// Allan CORNET - 2012
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////
#include "Variance.h"
#include "gw_ipd.h"
#include "IPD.h"
extern "C"
{
#include "api_scilab.h"
#if API_SCILAB_VERSION < 3
#include "stack-c.h"
#endif
#include "Scierror.h"
};



template<class GrayValueType>
GrayValueType* VarianceFilter(const GrayValueType* const OriginalImage,
    int NumberOfFilterColumns,
    int NumberOfFilterRows,
    int NumberOfImageColumns,
    int NumberOfImageRows);


int sci_VarianceFilter(char* fname)
{
    // Input and output parameters are checked.

    CheckRhs(2, 2);

    CheckLhs(1, 1);

    // The first argument is a 2D matrix of type uint8, uint16, uint32 or double.

    int* OriginalImageAddress = NULL;

    SciErr ScilabError = getVarAddressFromPosition(pvApiCtx, 1, &OriginalImageAddress);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    int ImageType = 0;

    int Precision = 0;

    ScilabError = getVarType(pvApiCtx, OriginalImageAddress, &ImageType);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    switch(ImageType)
    {
    case sci_matrix:
        {
            // Okay.
        } // case

        break;

    case sci_boolean:
        {
            // Okay.
        } // case

        break;

    case sci_ints:
        {
            // Only unsigned integer images are processed by IPD toolbox.

            ScilabError = getMatrixOfIntegerPrecision(pvApiCtx, OriginalImageAddress, &Precision);

            if(ScilabError.iErr)
            {
                printError(&ScilabError, 0);

                return 0;
            } // if

            switch(Precision)
            {
            case SCI_UINT8:
                {
                    // Okay.
                } // case

                break;

            case SCI_UINT16:
                {
                    // Okay.
                } // case

                break;

            case SCI_UINT32:
                {
                    // Okay.
                } // case

                break;

            default:
                {
                    string Message1("The first argument must be a matrix of type boolean, uint8, uint16, ");

                    string Message2("uint32 or double.");

                    string Message(Message1 + Message2);

                    Scierror(ERROR_ARGUMENT, const_cast<char*>(Message.data()));

                    return 0;
                } // default

                break;
            } // switch
        } // case

        break;

    default:
        {
            string Message1("The first argument must be a matrix of type boolean, uint8, uint16, ");

            string Message2("uint32 or double.");

            string Message(Message1 + Message2);

            Scierror(ERROR_ARGUMENT, const_cast<char*>(Message.data()));

            return 0;
        } // default

        break;
    } // switch

    // The second argument must be a vector of type double. In Scilab the size of an object is
    // expressed as a vector of type double and this convention is followed.

    int* DimensionsAddress = NULL;

    ScilabError = getVarAddressFromPosition(pvApiCtx, 2, &DimensionsAddress);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    double* Dimensions = NULL;

    int NumberOfRows = 0;

    int NumberOfColumns = 0;

    ScilabError = getMatrixOfDouble(pvApiCtx,
        DimensionsAddress,
        &NumberOfRows,
        &NumberOfColumns,
        &Dimensions);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    if(!(  ((NumberOfRows == 2) & (NumberOfColumns == 1))
        | ((NumberOfRows == 1) & (NumberOfColumns == 2))))
    {
        Scierror(ERROR_ARGUMENT, "The second argument must be a vector of type double.");

        return 0;
    } // if

    // The image data are retrieved and the image is filtered.

    switch(ImageType)
    {
    case sci_matrix:
        {
            double* OriginalImage = NULL;

            ScilabError = getMatrixOfDouble(pvApiCtx,
                OriginalImageAddress,
                &NumberOfRows,
                &NumberOfColumns,
                &OriginalImage);

            if(ScilabError.iErr)
            {
                printError(&ScilabError, 0);

                return 0;
            } // if

            double* FilteredImage = VarianceFilter(OriginalImage,
                static_cast<int>(Dimensions[1]),
                static_cast<int>(Dimensions[0]),
                NumberOfColumns,
                NumberOfRows);

            if(FilteredImage == NULL)
            {
                Scierror(ERROR_MEMORY, "Internal error occured.");

                return 0;
            } // if

            ScilabError = createMatrixOfDouble(pvApiCtx,
                Rhs + 1,
                NumberOfRows,
                NumberOfColumns,
                FilteredImage);
        } // case

        break;

    case sci_boolean:
        {
            int* OriginalImage = NULL;

            ScilabError = getMatrixOfBoolean(pvApiCtx,
                OriginalImageAddress,
                &NumberOfRows,
                &NumberOfColumns,
                &OriginalImage);

            if(ScilabError.iErr)
            {
                printError(&ScilabError, 0);

                return 0;
            } // if

            int* FilteredImage = VarianceFilter(OriginalImage,
                static_cast<int>(Dimensions[1]),
                static_cast<int>(Dimensions[0]),
                NumberOfColumns,
                NumberOfRows);

            if(FilteredImage == NULL)
            {
                Scierror(ERROR_MEMORY, "Internal error occured.");

                return 0;
            } // if

            ScilabError = createMatrixOfBoolean(pvApiCtx,
                Rhs + 1,
                NumberOfRows,
                NumberOfColumns,
                FilteredImage);

            if(ScilabError.iErr)
            {
                printError(&ScilabError, 0);

                return 0;
            } // if
        } // case

        break;

    case sci_ints:
        {
            switch(Precision)
            {
            case SCI_UINT8:
                {
                    TUChar* OriginalImage = NULL;

                    ScilabError = getMatrixOfUnsignedInteger8(pvApiCtx,
                        OriginalImageAddress,
                        &NumberOfRows,
                        &NumberOfColumns,
                        &OriginalImage);

                    if(ScilabError.iErr)
                    {
                        printError(&ScilabError, 0);

                        return 0;
                    } // if

                    TUChar* FilteredImage = VarianceFilter(OriginalImage,
                        static_cast<int>(Dimensions[1]),
                        static_cast<int>(Dimensions[0]),
                        NumberOfColumns,
                        NumberOfRows);

                    if(FilteredImage == NULL)
                    {
                        Scierror(ERROR_MEMORY, "Internal error occured.");

                        return 0;
                    } // if

                    ScilabError = createMatrixOfUnsignedInteger8(pvApiCtx,
                        Rhs + 1,
                        NumberOfRows,
                        NumberOfColumns,
                        FilteredImage);

                    if(ScilabError.iErr)
                    {
                        printError(&ScilabError, 0);

                        return 0;
                    } // if
                } // case

                break;

            case SCI_UINT16:
                {
                    TUShort* OriginalImage = NULL;

                    ScilabError = getMatrixOfUnsignedInteger16(pvApiCtx,
                        OriginalImageAddress,
                        &NumberOfRows,
                        &NumberOfColumns,
                        &OriginalImage);

                    if(ScilabError.iErr)
                    {
                        printError(&ScilabError, 0);

                        return 0;
                    } // if

                    TUShort* FilteredImage = VarianceFilter(OriginalImage,
                        static_cast<int>(Dimensions[1]),
                        static_cast<int>(Dimensions[0]),
                        NumberOfColumns,
                        NumberOfRows);

                    if(FilteredImage == NULL)
                    {
                        Scierror(ERROR_MEMORY, "Internal error occured.");

                        return 0;
                    } // if

                    ScilabError = createMatrixOfUnsignedInteger16(pvApiCtx,
                        Rhs + 1,
                        NumberOfRows,
                        NumberOfColumns,
                        FilteredImage);

                    if(ScilabError.iErr)
                    {
                        printError(&ScilabError, 0);

                        return 0;
                    } // if
                } // case

                break;

            case SCI_UINT32:
                {
                    TUInt* OriginalImage = NULL;

                    ScilabError = getMatrixOfUnsignedInteger32(pvApiCtx,
                        OriginalImageAddress,
                        &NumberOfRows,
                        &NumberOfColumns,
                        &OriginalImage);

                    if(ScilabError.iErr)
                    {
                        printError(&ScilabError, 0);

                        return 0;
                    } // if

                    TUInt* FilteredImage = VarianceFilter(OriginalImage,
                        static_cast<int>(Dimensions[1]),
                        static_cast<int>(Dimensions[0]),
                        NumberOfColumns,
                        NumberOfRows);

                    if(FilteredImage == NULL)
                    {
                        Scierror(ERROR_MEMORY, "Internal error occured.");

                        return 0;
                    } // if

                    ScilabError = createMatrixOfUnsignedInteger32(pvApiCtx,
                        Rhs + 1,
                        NumberOfRows,
                        NumberOfColumns,
                        FilteredImage);

                    if(ScilabError.iErr)
                    {
                        printError(&ScilabError, 0);

                        return 0;
                    } // if
                } // case

                break;

            default:
                {
                } // default

                break;
            } // switch
        } // case

        break;

    default:
        {
        } // default

        break;
    } // switch

    // Output argument position is set.
    LhsVar(1) = Rhs + 1;

    return 0;
} // sci_VarianceFilter



template<class GrayValueType>
GrayValueType* VarianceFilter(const GrayValueType* const OriginalImage,
    int NumberOfFilterColumns,
    int NumberOfFilterRows,
    int NumberOfImageColumns,
    int NumberOfImageRows)
{
    CVariance<GrayValueType> Variance(NumberOfFilterColumns, NumberOfFilterRows);

    GrayValueType* FilteredImage = new GrayValueType[NumberOfImageRows * NumberOfImageColumns];

    if(FilteredImage != NULL)
    {
        Variance.FilterImage(NumberOfImageColumns, NumberOfImageRows, OriginalImage, FilteredImage);
    } // if

    return FilteredImage;
} // VarianceFilter
