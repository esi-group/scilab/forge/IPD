//////////////////////////////////////////////////////////////////////////// 
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////


#ifndef VARIANCE_H
#define VARIANCE_H


#include "MaskFilterTemplate.h"


template<class GrayValueType>
class CVariance : public CMaskFilterTemplate<GrayValueType>
{
public:

    CVariance(TUInt NumberOfColumns, TUInt NumberOfRows);

protected:

    typedef vector<GrayValueType> TGrayValueList;
    virtual GrayValueType ComputeGrayValue(const TGrayValueList& GrayValues) const;
};


template<class GrayValueType>
CVariance<GrayValueType>::CVariance(TUInt NumberOfColumns, TUInt NumberOfRows):
CMaskFilterTemplate<GrayValueType>(NumberOfColumns, NumberOfRows)
{
} // CVariance::CVariance


template<class GrayValueType>
GrayValueType CVariance<GrayValueType>::ComputeGrayValue(const TGrayValueList& GrayValues) const
{
    const GrayValueType StartValue = 0;

    return inner_product(GrayValues.begin(), GrayValues.end(), GrayValues.begin(), StartValue) / 
        static_cast<GrayValueType>(GrayValues.size());
} // CVariance::ComputeGrayValue

#endif
