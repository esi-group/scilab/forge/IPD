////////////////////////////////////////////////////////////////////////////
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////

#ifndef MORPHOLOGICAL_FILTER_TEMPLATE_H
#define MORPHOLOGICAL_FILTER_TEMPLATE_H

#include "IPD.h"
#include "Filter.h"


template<class GrayValueType>
class CMorphologicalFilter : public CFilter<GrayValueType>
{
public:

    CMorphologicalFilter(TUInt NumberOfColumns, TUInt NumberOfRows, const int* const Values);

    virtual ~CMorphologicalFilter(){};

protected:
    typedef vector<GrayValueType> TGrayValueList;
    virtual GrayValueType ComputeGrayValue(const TGrayValueList& GrayValues) const = 0;

    const int* const m_Values;
}; // CMorphologicalFilter


template<class GrayValueType>
CMorphologicalFilter<GrayValueType>::CMorphologicalFilter(TUInt NumberOfColumns,
    TUInt NumberOfRows,
    const int* const Values):
CFilter<GrayValueType>(NumberOfColumns, NumberOfRows),
    m_Values(Values)
{
    // Central pixel coordinates are calculated.

    TUInt CentralColumn = NumberOfColumns / 2;

    TUInt CentralRow = NumberOfRows / 2;

    // For each non-zero pixel of the filter the horizontal and vertical distances to the cen-
    // tral pixel are calculated and stored in m_ColumnOffsets and m_RowOffsets respectively.

    TUInt ElementIndex = 0;

    for(TUInt n = 0; n < NumberOfColumns; n++)
    {
        for(TUInt m = 0; m < NumberOfRows; m++)
        {
            if(m_Values[ElementIndex] > 0)
            {
                CMorphologicalFilter<GrayValueType>::m_ColumnOffsets.push_back(n - CentralColumn);

                CMorphologicalFilter<GrayValueType>::m_RowOffsets.push_back(m - CentralRow);

                CMorphologicalFilter<GrayValueType>::m_NumberOfElements++;
            } // if

            ElementIndex++;
        } // for
    } // for
} // CMorphologicalFilter::CMorphologicalFilter


#endif
