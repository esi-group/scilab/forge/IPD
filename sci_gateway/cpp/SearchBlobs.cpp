////////////////////////////////////////////////////////////////////////////
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
// Allan CORNET - 2012
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////

#include "gw_ipd.h"
#include "IPD.h"
extern "C"
{
#include "api_scilab.h"
#if API_SCILAB_VERSION < 3
    #include "stack-c.h"
#endif
#include "Scierror.h"
};

TUInt SearchRoot(TUInt Node, const TVector& SearchTree);


int sci_SearchBlobs(char* fname)
{
    // Input and output parameters are checked.

    CheckRhs(1, 1);

    CheckLhs(1, 1);

    // The input parameter is a matrix of type boolean.

    int* LogicalImageAddress = NULL;

    SciErr ScilabError = getVarAddressFromPosition(pvApiCtx, 1, &LogicalImageAddress);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    int NumberOfRows = 0;

    int NumberOfColumns = 0;

    int* LogicalImage = NULL;

    ScilabError = getMatrixOfBoolean(pvApiCtx,
        LogicalImageAddress,
        &NumberOfRows,
        &NumberOfColumns,
        &LogicalImage);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    // initializations

    TUInt NumberOfPixels = NumberOfRows * NumberOfColumns;

    TVector ObjectImage(NumberOfPixels, 0); // pixels arranged as in Scilab

    TVector SearchTree(1, 0); // first element is zero to avoid index arithmetics problems

    TUInt NextObjectLabel = 1;

    TUInt PixelIndex = 0;

    TUInt* BlobImage = new TUInt[NumberOfPixels];

    if(BlobImage == NULL)
    {
        Scierror(ERROR_MEMORY, "Internal error occured.");

        return 0;
    } // if

    for(TUInt n = 0; n < NumberOfPixels; n++)
    {
        BlobImage[n] = 0;
    } // for

    // For each true-valued pixel in LogicalImage a first label is searched.
    for(TUInt Column = 0; Column < NumberOfColumns; Column++)
    {
        for(TUInt Row = 0; Row < NumberOfRows; Row++)
        {
            if(LogicalImage[PixelIndex] > 0)
            {
                // The prior (upper, upper left, left, lower left) neighbors of current pixel
                // are checked.

                const TUShort NumberOfCheckedNeighbors = 4;

                TUInt ListOfNeighbors[4]; // list of labels at neighboring pixels

                TUShort NumberOfNeighbors = 0; // number of currently checked neighbor pixels

                // upper neighbor
                if((Row > 0) & (LogicalImage[PixelIndex - 1] > 0))
                {
                    ListOfNeighbors[NumberOfNeighbors] = ObjectImage[PixelIndex - 1];

                    NumberOfNeighbors++;
                } // if

                // upper left neighbor
                if((Column > 0) & (Row > 0))
                {
                    TUInt NeighborIndex = (Column - 1) * NumberOfRows + Row - 1;

                    if(LogicalImage[NeighborIndex] > 0)
                    {
                        ListOfNeighbors[NumberOfNeighbors] = ObjectImage[NeighborIndex];

                        NumberOfNeighbors++;
                    } // if
                } // if

                // left neighbor
                if(Column > 0)
                {
                    TUInt NeighborIndex = (Column - 1) * NumberOfRows + Row;

                    if(LogicalImage[NeighborIndex] > 0)
                    {
                        ListOfNeighbors[NumberOfNeighbors] = ObjectImage[NeighborIndex];

                        NumberOfNeighbors++;
                    } // if
                } // if

                // lower left neighbor
                if((Column > 0) & (Row < NumberOfRows - 1))
                {
                    TUInt NeighborIndex = (Column - 1) * NumberOfRows + Row + 1;

                    if(LogicalImage[NeighborIndex] > 0)
                    {
                        ListOfNeighbors[NumberOfNeighbors] = ObjectImage[NeighborIndex];

                        NumberOfNeighbors++;
                    } // if
                } // if

                // If ListOfNeighbors not empty, the minimum neighbor label is selected.
                // Otherwise the current pixel belongs to a new blob.
                if(NumberOfNeighbors > 0)
                {
                    TUInt CurrentLabel = ListOfNeighbors[0];

                    for(TUShort n = 1; n < NumberOfNeighbors; n++)
                    {
                        if(ListOfNeighbors[n] < CurrentLabel)
                        {
                            CurrentLabel = ListOfNeighbors[n];
                        } // if
                    } // for

                    ObjectImage[PixelIndex] = CurrentLabel;

                    // All prior neighbors of the current pixel shall have the same root as the current
                    // pixel.

                    TUInt CurrentRoot = SearchRoot(CurrentLabel, SearchTree);

                    for(TUShort n = 0; n < NumberOfNeighbors; n++)
                    {
                        TUInt NeighborRoot = SearchRoot(ListOfNeighbors[n], SearchTree);

                        if(NeighborRoot != CurrentRoot)
                        {
                            SearchTree[NeighborRoot] = CurrentRoot;
                        } // if
                    } // for

                    NumberOfNeighbors = 0;
                }
                else
                {
                    ObjectImage[PixelIndex] = NextObjectLabel;

                    SearchTree.push_back(NextObjectLabel);

                    NextObjectLabel++;
                } // if
            }
            else
            {
                BlobImage[PixelIndex] = 0;
            }// if

            // PixelIndex is incremented.
            PixelIndex++;
        } // for
    } // for

    // If any blobs were found, the adjacent blobs are merged and their labels are made
    // continous.
    if(NextObjectLabel > 1)
    {
        // Adjacent blobs are merged.
        for(TUInt PixelIndex = 0; PixelIndex < NumberOfPixels; PixelIndex++)
        {
            ObjectImage[PixelIndex] = SearchRoot(ObjectImage[PixelIndex], SearchTree);
        } // for

        // A sorted list of unique root labels is built.

        TList RootList;

        TVector::iterator TreeIndex;

        for(TreeIndex = SearchTree.begin(); TreeIndex != SearchTree.end(); TreeIndex++)
        {
            RootList.push_back(*TreeIndex);
        } // for

        RootList.sort();

        RootList.unique();

        TVector RootVector;

        TList::iterator ListIndex;

        for(ListIndex = RootList.begin(); ListIndex != RootList.end(); ListIndex++)
        {
            RootVector.push_back(*ListIndex);
        } // if

        for(TUInt PixelIndex = 0; PixelIndex < NumberOfPixels; PixelIndex++)
        {
            if(LogicalImage[PixelIndex] > 0)
            {
                TUInt BlobLabel = ObjectImage[PixelIndex];

                // Pixel is searched in RootVector.

                TUInt Start = 1;

                TUInt Stop = RootVector.size() - 1;

                while(Start < Stop)
                {
                    TUInt First = RootVector[Start];

                    TUInt Last = RootVector[Stop];

                    if((BlobLabel != First) & (BlobLabel != Last))
                    {
                        TUInt Mid = (Start + Stop) / 2;

                        if(BlobLabel <= RootVector[Mid])
                        {
                            Stop = Mid;
                        }
                        else
                        {
                            Start = Mid;
                        } // if
                    }
                    else
                    {
                        if(BlobLabel == First)
                        {
                            Stop = Start;
                        }
                        else
                        {
                            Start = Stop;
                        }// if
                    } // if
                } // while

                BlobImage[PixelIndex] = Start;
            } // if
        } // for
    } // if

    // Output variable is created.

    ScilabError = createMatrixOfUnsignedInteger32(pvApiCtx,
        Rhs + 1,
        NumberOfRows,
        NumberOfColumns,
        BlobImage);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    LhsVar(1) = Rhs + 1;

    return 0;
} // sci_SearchBlobs



TUInt SearchRoot(TUInt Node, const TVector& SearchTree)
{
    TUInt NextNode = SearchTree[Node];

    TUInt Root = Node;

    while(NextNode != Root)
    {
        Root = NextNode;

        NextNode = SearchTree[Root];
    } // while

    return Root;
} // SearchRoot
