////////////////////////////////////////////////////////////////////////////
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
// Allan CORNET - 2012
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////

#include "gw_ipd.h"
#include "IPD.h"
extern "C"
{
#include "api_scilab.h"
#if API_SCILAB_VERSION < 3
#include "stack-c.h"
#endif
#include "Scierror.h"
};


int sci_CreatePixelIndexList(char *fname)
{
    // Input and output parameters are checked.

    CheckRhs(1, 1);

    CheckLhs(1, 1);

    // The input parameter is a blob image, i. e. a matrix of type uint32.

    int* BlobImageAddress = NULL;

    SciErr ScilabError = getVarAddressFromPosition(pvApiCtx, 1, &BlobImageAddress);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    int Precision = 0;

    ScilabError = getMatrixOfIntegerPrecision(pvApiCtx, BlobImageAddress, &Precision);

    if(ScilabError.iErr || (Precision != SCI_UINT32))
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    int NumberOfRows = 0;

    int NumberOfColumns = 0;

    TUInt* BlobImage = NULL;

    ScilabError = getMatrixOfUnsignedInteger32(pvApiCtx,
        BlobImageAddress,
        &NumberOfRows,
        &NumberOfColumns,
        &BlobImage);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    // Number of objects is determined.

    TUInt NumberOfObjects = 0;

    TUInt NumberOfPixels = NumberOfRows * NumberOfColumns;

    for(TUInt n = 0; n < NumberOfPixels; n++)
    {
        if(BlobImage[n] > NumberOfObjects)
        {
            NumberOfObjects = BlobImage[n];
        } // if
    } // for

    // If there are no objects in the image, an empty matrix is returned.

    if(NumberOfObjects == 0)
    {
        ScilabError = createMatrixOfUnsignedInteger32(pvApiCtx, Rhs + 1, 0, 0, NULL);

        if(ScilabError.iErr)
        {
            printError(&ScilabError, 0);
        } // if

        LhsVar(1) = Rhs + 1;

        return 0;
    } // if


    // The size of the largest object is determined. For this purpose an object histogram is
    // calculated. The maximum entry of this histogram is the maximum area.

    TVector Histogram(NumberOfObjects);

    for(TUInt n = 0; n < NumberOfPixels; n++)
    {
        if(BlobImage[n] > 0)
        {
            Histogram[BlobImage[n] - 1]++;
        } // if
    } // for

    TUInt MaxArea = 0;

    for(TUInt n = 0; n < NumberOfObjects; n++)
    {
        if(Histogram[n] > MaxArea)
        {
            MaxArea = Histogram[n];
        } // if
    } // for

    // PixelIndexMatrix is allocated and initialized.

    TUInt NumberOfElements = NumberOfObjects * MaxArea;

    TUInt* PixelIndexMatrix = new TUInt[NumberOfElements];

    if(PixelIndexMatrix == NULL)
    {
        Scierror(ERROR_MEMORY, "Internal error occured.");

        ScilabError = createMatrixOfUnsignedInteger32(pvApiCtx, Rhs + 1, 0, 0, NULL);

        if(ScilabError.iErr)
        {
            printError(&ScilabError, 0);
        } // if

        return 0;
    } // if

    for(TUInt n = 0; n < NumberOfElements; n++)
    {
        PixelIndexMatrix[n] = 0;
    } // for

    // For each column of PixelIndexMatrix an index to the next element is initialized.
    TVector NextElement(NumberOfObjects, 0);

    // The pixel indices are transferred to PixelIndexMatrix.
    for(TUInt n = 0; n < NumberOfPixels; n++)
    {
        if(BlobImage[n] > 0)
        {
            TUInt BlobIndex = BlobImage[n] - 1;

            PixelIndexMatrix[BlobIndex * MaxArea + NextElement[BlobIndex]] = n + 1;

            NextElement[BlobIndex]++;
        } // if
    } // for

    ScilabError = createMatrixOfUnsignedInteger32(pvApiCtx,
        Rhs + 1,
        MaxArea,
        NumberOfObjects,
        PixelIndexMatrix);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    LhsVar(1) = Rhs + 1;

    return 0;
} // CreatePixelIndexList
