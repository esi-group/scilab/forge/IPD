////////////////////////////////////////////////////////////////////////////
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
// Allan CORNET - 2012
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////
#include "ScilabToIplImage.h"
#include "IplImageToScilab.h"
#include "TransferImageData.h"

#include "gw_ipd.h"
#include "IPD.h"
extern "C"
{
#include "api_scilab.h"
#if API_SCILAB_VERSION < 3
#include "stack-c.h"
#endif
#include "Scierror.h"
};


 int sci_Watershed(char* fname)
 {
  // Input and output parameters are checked.

  CheckRhs(2, 2);

  CheckLhs(1, 1);

  // The first argument is a 2D matrix of type uint8.

  int* Address = NULL;

  SciErr ScilabError = getVarAddressFromPosition(pvApiCtx, 1, &Address);

  if(ScilabError.iErr)
   {
     printError(&ScilabError, 0);

     return 0;
   } // if

  TUChar* SourceImageData = NULL;

  int NumberOfRows = 0;

  int NumberOfColumns = 0;

  ScilabError = getMatrixOfUnsignedInteger8(pvApiCtx,
                                            Address,
                                            &NumberOfRows,
                                            &NumberOfColumns,
                                            &SourceImageData);

  if(ScilabError.iErr)
   {
     printError(&ScilabError, 0);

     return 0;
   } // if

  // The second argument is a 2D matrix of type uint32 or int32.

  ScilabError = getVarAddressFromPosition(pvApiCtx, 2, &Address);

  if(ScilabError.iErr)
   {
     printError(&ScilabError, 0);

     return 0;
   } // if

  int Precision = 0;

  ScilabError = getMatrixOfIntegerPrecision(pvApiCtx, Address, &Precision);

  if(ScilabError.iErr)
   {
     printError(&ScilabError, 0);

     return 0;
   } // if

  if((Precision != SCI_UINT32) && (Precision != SCI_INT32))
  {
   Scierror(ERROR_ARGUMENT,
            "The second argument must be a 2D matrix of type uint32 or int32.");

   return 0;
  } // if

  int NumberOfMarkerRows = 0;

  int NumberOfMarkerColumns = 0;

  ScilabError = getVarDimension(pvApiCtx,
                                Address,
                                &NumberOfMarkerRows,
                                &NumberOfMarkerColumns);

  if(ScilabError.iErr)
   {
     printError(&ScilabError, 0);

     return 0;
   } // if

  if((NumberOfMarkerRows != NumberOfRows) || (NumberOfMarkerColumns != NumberOfColumns))
  {
   Scierror(ERROR_ARGUMENT,
            "The second argument must have the same dimensions as the first one.");

   return 0;
  } // if

  // The source image must be transformed to a three channel image due to the implementation
  // of cvWatershed.

  IplImage* Source = NULL;

  ScilabToIplImage(&Source,
                   SourceImageData,
                   NumberOfColumns,
                   NumberOfRows,
                   1,
                   sci_ints,
                   SCI_UINT8);

  if(Source == NULL)
  {
   Scierror(ERROR_MEMORY, "Internal error occured.");

   return 0;
  } // if

  IplImage* Image = cvCreateImage(cvSize(NumberOfColumns, NumberOfRows), IPL_DEPTH_8U, 3);

  if(Image == NULL)
  {
   Scierror(ERROR_MEMORY, "Internal error occured.");

   cvReleaseImage(&Source);

   return 0;
  } // if

  cvCvtColor(Source, Image, CV_GRAY2BGR);

  cvReleaseImage(&Source);

  // The marker image is transformed to OpenCV format. The depth is IPL_DEPTH_32S.

  IplImage* Marker = NULL;

  int* MarkerImageData = NULL;

  bool ToBeDeleted = true;

  switch(Precision)
  {
   case SCI_UINT32:
   {
    TUInt* ImageDataAsUInt32 = NULL;

    ScilabError = getMatrixOfUnsignedInteger32(pvApiCtx,
                                               Address,
                                               &NumberOfMarkerRows,
                                               &NumberOfMarkerColumns,
                                               &ImageDataAsUInt32);

    if(ScilabError.iErr)
     {
       printError(&ScilabError, 0);

       return 0;
     } // if

    TransferImageData(NumberOfMarkerRows * NumberOfMarkerColumns,
                      ImageDataAsUInt32,
                      &MarkerImageData);

    if(MarkerImageData == NULL)
    {
     Scierror(ERROR_MEMORY, "Internal error occured.");

     return 0;
    } // if
   } // case

   break;

   case SCI_INT32:
   {
    ToBeDeleted = false; // MarkerImageData points to Scilab memory.

    ScilabError = getMatrixOfInteger32(pvApiCtx,
                                       Address,
                                       &NumberOfMarkerRows,
                                       &NumberOfMarkerColumns,
                                       &MarkerImageData);

    if(ScilabError.iErr)
     {
       printError(&ScilabError, 0);

       return 0;
     } // if
   } // case

   break;

   default:
   {
   } // default

   break;
  } // switch

  if(MarkerImageData == NULL)
  {
   Scierror(ERROR_MEMORY, "Internal error occured.");

   return 0;
  } // if

  ScilabToIplImage(&Marker,
                   MarkerImageData,
                   NumberOfColumns,
                   NumberOfRows,
                   1,
                   sci_ints,
                   SCI_INT32);

  if(ToBeDeleted)
  {
   delete MarkerImageData;
  } // if

  if(Marker == NULL)
  {
   Scierror(ERROR_MEMORY, "Internal error occured.");

   return 0;
  } // if

  // Watershed transform is applied now.

  cvWatershed(Image, Marker);

  cvReleaseImage(&Image);

  // The output argument is generated. The watershed transform result is stored in Marker.
  // Marker is transferred to a Scilab matrix of type int32.

  int* ImageData = NULL;

  IplImageToScilab(Marker, &ImageData, COLOR_SPACE_GRAY);

  cvReleaseImage(&Marker);

  if(ImageData == NULL)
  {
   Scierror(ERROR_MEMORY, "Internal error occured.");

   return 0;
  } // if

  ScilabError = createMatrixOfInteger32(pvApiCtx,
                                        Rhs + 1,
                                        NumberOfRows,
                                        NumberOfColumns,
                                        ImageData);

  if(ScilabError.iErr)
  {
   printError(&ScilabError, 0);

   return 0;
  } // if

  // Output argument position is set.
  LhsVar(1) = Rhs + 1;

  return 0;
 } // sci_Watershed
