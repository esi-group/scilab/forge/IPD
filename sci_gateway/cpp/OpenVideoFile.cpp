////////////////////////////////////////////////////////////////////////////
// IPD - Image Processing Design Toolbox
//
// Copyright (c) by Dr. Eng. (J) Harald Galda, 2009 - 2011
// Allan CORNET - 2012
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
////////////////////////////////////////////////////////////////////////////

#include "gw_ipd.h"
#include "IPD.h"
extern "C"
{
#include "api_scilab.h"
#if API_SCILAB_VERSION < 3
#include "stack-c.h"
#endif
#include "Scierror.h"
};


int sci_OpenVideoFile(char* fname)
{
    // Input and output parameters are checkded.

    CheckRhs(1, 1);

    CheckLhs(1, 1);

    // The input parameter is the name of the video file to be opened.

    // Address of input parameter (file name) is determined.

    SciErr ScilabError;

    int* FileNamePointer = NULL;

    ScilabError = getVarAddressFromPosition(pvApiCtx, 1, &FileNamePointer);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    // Length of file name is determined. An input parameter of type string can be a matrix with
    // several rows and columns. Each element of this matrix is a string. Therefore, the number
    // of columns and rows are checked. A file name should consist of only one single string, so
    // the function returns otherwise.

    int NumberOfRows = 0;

    int NumberOfColumns = 0;

    int StringLength = 0;

    ScilabError = getMatrixOfString(pvApiCtx,
        FileNamePointer,
        &NumberOfRows,
        &NumberOfColumns,
        NULL,
        NULL);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    if((NumberOfRows != 1) || (NumberOfColumns != 1))
    {
        Scierror(ERROR_FILE_NAME, "Invalid file name. A file name must be a string.");

        return 0;
    } // if

    // Length of file name is determined.

    ScilabError = getMatrixOfString(pvApiCtx,
        FileNamePointer,
        &NumberOfRows,
        &NumberOfColumns,
        &StringLength,
        NULL);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    // File name is retrieved.

    char* FileName = new char[StringLength + 1];

    if(FileName == NULL)
    {
        Scierror(ERROR_MEMORY, "Internal error occured.");

        return 0;
    } // if

    ScilabError = getMatrixOfString(pvApiCtx,
        FileNamePointer,
        &NumberOfRows,
        &NumberOfColumns,
        &StringLength,
        &FileName);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        delete FileName;

        return 0;
    } // if

    // The video file with the specified file name is opened.

    CvCapture* VideoFile = cvCreateFileCapture(FileName);

    delete FileName;

    if(VideoFile == NULL)
    {
        Scierror(ERROR_MEMORY, "Internal error occured.");

        return 0;
    } // if

    // The output argument is a pointer to the file.

    ScilabError = createPointer(pvApiCtx, Rhs + 1, (void*) VideoFile);

    if(ScilabError.iErr)
    {
        printError(&ScilabError, 0);

        return 0;
    } // if

    // Output argument position is set.
    LhsVar(1) = Rhs + 1;

    return 0;
} // sci_OpenVideoFile
