// Allan CORNET - 2012
//
// unit test
//
RGB = rand(3, 3, 3); // generate random image
[IndexedImage, ColorMap] = RGB2Ind(RGB); // RGB is converted to an indexed image
FigureHandle = ShowImage(IndexedImage, 'Example', ColorMap); // IndexedImage is displayed