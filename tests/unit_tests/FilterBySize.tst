// Allan CORNET - 2012
//
// unit test
//
Image = rand(9, 9); // generate random image
Threshold = CalculateOtsuThreshold(Image); // calculate a threshold
BinaryImage = SegmentByThreshold(Image, Threshold); // segment image
BlobImage = SearchBlobs(BinaryImage); // blob image is generated
FilteredBlobImage = FilterBySize(BlobImage, 3, 60)