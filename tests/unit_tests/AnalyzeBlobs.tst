// Allan CORNET - 2012
//
// unit test AnalyzeBlobs
//
global IPD_PATH;
RGB = ReadImage(IPD_PATH + 'demos\teaset.png');
Image = RGB2Gray(RGB);
ThresholdedImage = SegmentByThreshold(Image, 200);
BlobImage = SearchBlobs(ThresholdedImage);
FilteredBlobImage = FilterBySize(BlobImage, 100); // Small objects are removed.
IsCalculated = CreateFeatureStruct(%f); // Feature struct is generated.
IsCalculated.BoundingBox = %t; // The bounding box shall be calculated for each blob.
BlobStatistics = AnalyzeBlobs(FilteredBlobImage, IsCalculated);
assert_checkequal(size(BlobStatistics), 8);