// Allan CORNET - 2012
//
// unit test
//
global IPD_PATH;
VideoPath = IPD_PATH + 'demos\red-car-video.avi';
VideoInfo = GetVideoStruct(VideoPath);
VideoFilePointer = OpenVideoFile(VideoPath);
figure();
for n = 1 : VideoInfo.NumberOfFrames
 RGB = ReadImage(VideoFilePointer);
 ShowColorImage(RGB, VideoPath);
end;
CloseVideoFile(VideoFilePointer);
