// Allan CORNET - 2012
//
// unit test
//
global IPD_PATH;
RGB = ReadImage(IPD_PATH + 'demos\teaset.png');
Image = RGB2Gray(RGB);
FilteredImage = VarianceFilter(Image, [9 9]);
figure();
ShowImage(Image, 'Original Image');
figure();
ShowImage(FilteredImage, 'Filtered Image');