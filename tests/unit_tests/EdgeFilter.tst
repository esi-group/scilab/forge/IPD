// Allan CORNET - 2012
//
// unit test
//
global IPD_PATH;
RGB = ReadImage(IPD_PATH + 'demos\teaset.png');
Image = RGB2Gray(RGB);
global EDGE_SOBEL;
Gradient = EdgeFilter(Image, EDGE_SOBEL);
figure();
ShowImage(Gradient, 'Gradient Image');