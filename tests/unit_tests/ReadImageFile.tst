// Allan CORNET - 2012
//
// unit test
//
global IPD_PATH;
[PixelList Dimensions] = ReadImageFile(IPD_PATH + 'demos\teaset.png');
RGB = matrix(PixelList, Dimensions);
ShowColorImage(RGB, 'Result');
