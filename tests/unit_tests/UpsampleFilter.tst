// Allan CORNET - 2012
//
// unit test
//
global WAVELET_DAUBECHIES_2;
FilterStruct = GetWaveletFilters(WAVELET_DAUBECHIES_2);
ExpandedLowPass = UpsampleFilter(FilterStruct.LowPass)
ExpandedHighPass = UpsampleFilter(FilterStruct.HighPass)