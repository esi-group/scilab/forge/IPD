// Allan CORNET - 2012
//
// unit test
//
global IPD_PATH;
RGB = ReadImage(IPD_PATH + 'demos\teaset.png');
Image = RGB2Gray(RGB);
global EDGE_SOBEL;
Gradient = EdgeFilter(Image, EDGE_SOBEL);
EdgeImage = ~SegmentByThreshold(Gradient, 60);
DistanceImage = DistanceTransform(EdgeImage);
ThresholdImage = SegmentByThreshold(DistanceImage, 8);
MarkerImage = SearchBlobs(ThresholdImage);
SegmentedImage = Watershed(Gradient, MarkerImage);
figure();
ColorMapLength = length(unique(SegmentedImage));
ShowImage(SegmentedImage, ...
          'Result of Watershed Transform', ...
          jetcolormap(ColorMapLength));
