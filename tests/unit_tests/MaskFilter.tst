// Allan CORNET - 2012
//
// unit test
//
Image = zeros(8, 8);
Image(5 : 8, :) = 1;
Mask = [-1 -1 -1; 0 0 0; 1 1 1];
FilteredImage = MaskFilter(Image, Mask)