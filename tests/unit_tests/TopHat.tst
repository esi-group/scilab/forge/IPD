// Allan CORNET - 2012
//
// unit test
//
Image = 0.2 * ones(9, 9); // generate a dark image
Image(:, 5) = 1; // draw a light line
StructureElement = CreateStructureElement('square', 3); // create structuring element
ResultImage = TopHat(Image, StructureElement)