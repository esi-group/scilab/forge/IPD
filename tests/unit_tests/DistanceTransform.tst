// Allan CORNET - 2012
//
// unit test
//
global IPD_PATH;
RGB = ReadImage(IPD_PATH + 'demos\teaset.png');
Image = RGB2Gray(RGB);
global EDGE_SOBEL;
Gradient = EdgeFilter(Image, EDGE_SOBEL);
EdgeImage = ~SegmentByThreshold(Gradient, 60);
DistanceImage = DistanceTransform(EdgeImage);
figure();
ShowImage(DistanceImage, 'Result of Distance Transform', jetcolormap(256));
