// Allan CORNET - 2012
//
// unit test BottomHat
//

Image = 0.8 * ones(9, 9); // generate a light image
Image(:, 5) = 0; // draw a dark line
StructureElement = CreateStructureElement('square', 3) // create structuring element
ResultImage = BottomHat(Image, StructureElement)
