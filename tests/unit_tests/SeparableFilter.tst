// Allan CORNET - 2012
//
// unit test
//
Image = zeros(8, 8);
Image(5 : 8, :) = 1;
ColumnFilter = [-1; 0; 1]/ sqrt(3);
RowFilter = [1 1 1]/ sqrt(3);
FilteredImage = SeparableFilter(Image, ColumnFilter, RowFilter)